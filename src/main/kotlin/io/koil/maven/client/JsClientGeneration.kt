package io.koil.maven.client

import com.squareup.kotlinpoet.ClassName
import io.koil.maven.common.GeneratorContext
import io.koil.maven.common.SchemaTypeInfo
import io.koil.schemas.ObjectJsonSchema
import io.koil.schemas.StringJsonSchema
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.RequestParameterType
import io.koil.web.webdef.EndpointContentType

// TODO Escape JS strings

fun generateJsClient(apiDefinitionClass: ClassName, ctx: GeneratorContext, endpoints: List<Endpoint>): String {
    val file = StringBuilder()
        .append("import type { Failable, HttpRequest, ApiError, TypedStringJsonSchema, TypedFloatJsonSchema, TypedIntegerJsonSchema, TypedObjectJsonSchema, TypedArrayJsonSchema, ApiClientOpts } from '@atlads/atlads'\n")
        .append("import { normalizeBaseUrl, sendRequest } from '@atlads/atlads'\n\n")

    // Generate objects interfaces
    // We need to keep track of inter-object dependencies to avoid getting 'used before declaration' warnings  on schemas
    // And we need to track object-likes like enums to make sure we output them before usage, and we don"t output them multiple times
    // Warning: we need to make sure we don't fall into the circular dependency trap
    for (type in ctx.schemaRegistry.registeredSchemas) {
        writeJsType(file, ctx, type)
    }

    // Create the main api client class
    file.append("export namespace ").append(apiDefinitionClass.simpleName).append("Client {\n")
        .append("  export const opts: ApiClientOpts = {\n")
        .append("    setBaseUrl(baseUrl: string): void {\n")
        .append("      this.baseUrl = normalizeBaseUrl(baseUrl)\n")
        .append("    }\n")
        .append("  }\n")
    for (endpoint in endpoints) {
        generateFunction(ctx, file, endpoint)
    }
    file.append("}\n")

    return file.toString()
}

private fun writeJsType(file: StringBuilder, ctx: GeneratorContext, type: SchemaTypeInfo) {
    if (type.written) return
    else if (type is SchemaTypeInfo.SubType) return

    for (dependency in type.dependsOn) {
        writeJsType(file, ctx, ctx.schemaRegistry.get(dependency) ?: continue)
    }

    val struct = when (type) {
        is SchemaTypeInfo.Enum -> generateEnum(ctx, type.schema)
        is SchemaTypeInfo.Obj -> generateType(ctx, type.schema, type.input, null)
        else -> throw RuntimeException("Unknown type $type")
    }

    file.append(struct).append("\n")
    type.written = true
}

// This could be optimized (maybe ?)
private fun generateQueryString(sb: StringBuilder, endpoint: Endpoint) {
    sb.append("    const queryParams: string[] = []\n")
    for (param in endpoint.params) {
        if (param.type !== RequestParameterType.QUERY) continue

        val name = param.name.trim()

        if (param.required) sb.append("    ")
        else sb.append("    if (!!").append(name).append(") ")

        sb.append("queryParams.push('").append(name).append("=' + ")

        // TODO Handle arrays/objects
        val schema = param.schema
        if (schema is StringJsonSchema) {
            // TODO Handle mapped schemas
            sb.append("encodeURIComponent(").append(name).append(")")
        } else {
            sb.append(name)
        }
        sb.append(")\n")
    }
    sb.append("    const queryString = queryParams.length ? '?' + queryParams.filter(p => !!p).join('&') : ''\n")
}

private fun addHeaderParams(sb: StringBuilder, endpoint: Endpoint): String {
    // TODO Header param name normalization

    val block = StringBuilder()

    for (param in endpoint.params) {
        if (param.type !== RequestParameterType.HEADER) continue
        if (param.required) {
            sb.append("        '").append(param.name).append("': ").append(param.name).append(",\n")
        } else {
            block.append("    if (!!").append(param.name).append(") request.headers['").append(param.name)
                .append("'] = ").append(param.name).append("\n")
        }
    }

    return block.toString()
}

private fun generateFunction(ctx: GeneratorContext, sb: StringBuilder, endpoint: Endpoint) {
    if (endpoint.hidden) return

    // Function description
    sb.append("\n  /**\n")
    val descriptionLines = endpoint.description.split("\n")
    for (line in descriptionLines) {
        sb.append("   * ").append(line.trim()).append("\n")
    }
    if (endpoint.requestBody != null) sb.append("   * @param requestBody The request body\n")
    for (param in endpoint.params) {
        sb.append("   * @param ").append(param.name).append(" ").append(param.description).append("\n")
    }
    sb.append("   * @returns ").append(endpoint.mainResponse.description).append("\n")
    sb.append("   */\n")

    // Prototype
    sb.append("  export function ")
        .append(endpoint.methodName)
        .append(" (\n")

    // Request body param
    // TODO Take other types of request bodies into account
    if (endpoint.requestBody.schema != null) {
        sb.append("    requestBody: ").append(ctx.resolveJsType(endpoint.requestBody.schema!!)).append(",\n")
    }

    var resolvedPath = endpoint.path
    var requiredParams = false
    var hasQueryParams = false
    for (param in endpoint.params) {
        val optional = !param.required
        requiredParams = requiredParams || param.required

        if (param.type == RequestParameterType.QUERY) hasQueryParams = true
        else if (param.type == RequestParameterType.PATH) {
            resolvedPath = resolvedPath.replace(":${param.name}", "' + ${param.name} + '")
        }

        // TODO Cookie param type
        // TODO Header param name normalization

        sb.append("    ").append(param.name)
        if (optional) sb.append("?")
        sb.append(": ").append(ctx.resolveJsType(param.schema)).append(",\n")
    }

    val mainReturnType =
        if (endpoint.mainResponse.schema == null) when (endpoint.mainResponse.type) {
            EndpointContentType.JSON -> "any"
            EndpointContentType.TEXT -> "string"
            EndpointContentType.BINARY -> "Blob"
            EndpointContentType.NONE -> "void"
        }
        else ctx.resolveJsType(endpoint.mainResponse.schema!!)

    sb.append("  ): Promise<Failable<").append(mainReturnType).append(", ApiError>> {\n")

    // Function body
    if (hasQueryParams) generateQueryString(sb, endpoint)

    // Prepare the request params
    sb.append("    const request: HttpRequest = {\n")
        .append("      method: '").append(endpoint.method.name()).append("',\n")
        .append("      path: '/").append(endpoint.namespace).append("/v").append(endpoint.apiVersion)
        .append(resolvedPath).append("'")
    if (hasQueryParams) sb.append(" + queryString")
    sb.append(",\n")

    if (endpoint.requestBody != null) sb.append("      body: requestBody,\n")
    else sb.append("      body: null,\n")

    sb.append("      headers: {\n")
    val headerParamsBlock = addHeaderParams(sb, endpoint)
    sb.append("      },\n")
        .append("    }\n")
        .append(headerParamsBlock)

    // Send the request
    sb.append("    return sendRequest(opts, '")
        .append(endpoint.operationId)
        .append("', request, ")
        .append(endpoint.mainResponse.status)
        .append(", ")
        .append(
            when (endpoint.mainResponse.type) {
                EndpointContentType.JSON -> "response => response.json()"
                EndpointContentType.TEXT -> "response => response.text()"
                EndpointContentType.BINARY -> "response => response.blob()"
                EndpointContentType.NONE -> "() => Promise.resolve()"
            }
        )
        .append(")\n  }\n")
}

private fun generateEnum(ctx: GeneratorContext, schema: StringJsonSchema): String {
    if (schema.enumSourceName == null) return ""

    // We do 2 things here
    // 1. Create a typescript enum
    // 2. Create an array with all possible values

    return StringBuilder("export type ")
        .append(ctx.namePrefix)
        .append(schema.enumSourceName!!)
        .append(" = ")
        .append(schema.enumValues!!.joinToString(" | ") { "'$it'" })
        .append("\nexport const ")
        .append(ctx.namePrefix)
        .append(schema.enumSourceName!!)
        .append("Keys: ")
        .append(ctx.namePrefix)
        .append(schema.enumSourceName!!)
        .append("[] = ")
        .append(schema.enumValues!!.joinToString(", ", prefix = "[", postfix = "]") { "'$it'" })
        .toString()
}

private fun generateSubType(
    ctx: GeneratorContext,
    generateSchema: Boolean,
    supertype: ObjectJsonSchema,
    subType: ObjectJsonSchema,
): String {
    return generateType(
        ctx,
        subType,
        generateSchema,
        supertype,
    )
}

private fun generateType(
    ctx: GeneratorContext,
    type: ObjectJsonSchema,
    generateSchema: Boolean,
    parent: ObjectJsonSchema?,
): String {
    val sb = StringBuilder()
    if (type.subTypes != null) {
        // TODO Recursive polymorphic types can't work
        // We would need to append all props from all parents for it to work the way we do

        val subTypeList = mutableListOf<String>()
        for (subType in type.subTypes!!) {
            val resolvedSubType = ctx.resolveSubType(type, subType)
            subTypeList.add(ctx.namePrefix + resolvedSubType.sourceName)
            sb.append(generateSubType(ctx, generateSchema, type, resolvedSubType))
        }

        sb.append("export type ")
            .append(ctx.namePrefix)
            .append(type.sourceName)
            .append(" = ")
            .append(subTypeList.joinToString(" | "))
            .append("\n")

        sb.append("export interface I")
            .append(ctx.namePrefix)
            .append(type.sourceName)
            .append(" {\n")

        // TODO This is duplicated code
        for (prop in type.properties) {
            if (prop.schema.description != null) {
                sb.append("  /**\n")
                val lines = prop.schema.description!!.split("\n")
                for (line in lines) {
                    sb.append("   * ").append(line).append("\n")
                }
                sb.append("   */\n")
            }

            sb.append("  ").append(prop.name)
            if (prop.nullable) sb.append("?")
            sb.append(": ").append(ctx.resolvePropertyJsType(type, prop.schema)).append("\n")
        }
        sb.append("}\n")

        if (generateSchema) {
            generateInterfaceSchema(ctx, sb, type, null)
        }

        return sb.toString()
    }

    // Add the interface doc
    val description = type.description
    if (description != null) {
        sb.append("/**\n")
        val descriptionLines = description.split("\n")
        for (line in descriptionLines) {
            sb.append(" * ").append(line).append("\n")
        }
        sb.append(" */\n")
    }

    // Interface declaration
    sb.append("export interface ")
    if (type.additionalPropertiesValues != null) sb.append("I")
    sb.append(ctx.namePrefix).append(type.sourceName).append(" {\n")

    if (parent != null) {
        parent.properties.map {
            if (it.polymorphicKey) it.name + ": '" + type.polymorphicValue!! + "'"
            else it.name + (if (it.nullable) "?: " else ": ") + ctx.resolvePropertyJsType(type, it.schema)
        }.forEach {
            sb.append("  ").append(it).append("\n")
        }
    }

    // Interface members
    for (prop in type.properties) {
        if (prop.schema.description != null) {
            sb.append("  /**\n")
            val lines = prop.schema.description!!.split("\n")
            for (line in lines) {
                sb.append("   * ").append(line).append("\n")
            }
            sb.append("   */\n")
        }

        sb.append("  ").append(prop.name)
        if (prop.nullable) sb.append("?")
        sb.append(": ").append(ctx.resolvePropertyJsType(type, prop.schema)).append("\n")
    }

    sb.append("}\n")

    // Use a union for additional properties to avoid messing with existing object keys
    if (type.additionalPropertiesKeys != null && type.additionalPropertiesValues != null) {
        sb.append("export type ")
            .append(ctx.namePrefix)
            .append(type.sourceName)
            .append(" = I")
            .append(ctx.namePrefix)
            .append(type.sourceName)
            .append(" & { [key: ")
            .append(ctx.resolvePropertyJsType(type, type.additionalPropertiesKeys!!))
            .append("]: ")
            .append(ctx.resolvePropertyJsType(type, type.additionalPropertiesValues!!))
            .append(" }\n")
    }

    // Add the interface schema
    if (generateSchema) {
        generateInterfaceSchema(ctx, sb, type, parent)
    }

    return sb.toString()
}

private fun generateInterfaceSchema(
    ctx: GeneratorContext,
    sb: StringBuilder,
    obj: ObjectJsonSchema,
    parent: ObjectJsonSchema?,
) {
    sb.append("export const ").append(ctx.namePrefix).append(obj.sourceName).append("Schema: TypedObjectJsonSchema<")

    // Use the interface instead of the union to avoid putting all fields of all sub-objects in the properties
    if (obj.subTypes != null) sb.append("I")

    sb.append(ctx.namePrefix).append(obj.sourceName).append("> = ")
    JsSchemaGenerator.outputObjectSchema(ctx, sb, obj, 1, parent)
    sb.append("\n")
}
