package io.koil.maven.client

import io.koil.maven.common.GeneratorContext
import io.koil.schemas.*

object JsSchemaGenerator {
    fun outputSchema(
        ctx: GeneratorContext,
        sb: StringBuilder,
        parent: ObjectJsonSchema,
        schema: JsonSchema,
        indentLevel: Int
    ) {
        when (schema) {
            is StringJsonSchema -> outputStringSchema(sb, schema, indentLevel)
            is IntegerJsonSchema -> outputIntegerSchema(sb, schema, indentLevel)
            is FloatJsonSchema -> outputFloatSchema(sb, schema, indentLevel)
            is BooleanJsonSchema -> outputBooleanSchema(sb, schema, indentLevel)
            is ArrayJsonSchema -> outputArraySchema(ctx, sb, parent, schema, indentLevel)
            is ObjectJsonSchema -> outputObjectSchema(ctx, sb, schema, indentLevel, null)
            is AnyJsonSchema -> outputAnySchema(sb, schema, indentLevel)
            is RefJsonSchema -> outputRefSchema(ctx, sb, parent, schema, indentLevel)
        }
    }

    private fun outputCommonSchema(sb: StringBuilder, schema: JsonSchema, indentLevel: Int) {
        sb.append(" ".repeat(indentLevel * 2)).append("type: '").append(schema.type).append("',\n")
        if (schema.description != null) {
            sb.append(" ".repeat(indentLevel * 2))
                .append("description: '")
                .append(
                    schema.description
                        ?.replace("\n", "\\n")
                        ?.replace("\"", "\\\"")
                        ?.replace("'", "\\'")
                )
                .append("',\n")
        }
    }

    fun outputStringSchema(sb: StringBuilder, schema: StringJsonSchema, indentLevel: Int) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)

        if (schema.format != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("format: '").append(schema.format).append("',\n")
        }
        if (schema.const != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("const: '").append(schema.const).append("',\n")
        }
        if (schema.minLength != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("minLength: ").append(schema.minLength).append(",\n")
        }
        if (schema.maxLength != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("maxLength: ").append(schema.maxLength).append(",\n")
        }
        if (schema.pattern != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("pattern: '").append(schema.pattern).append("',\n")
        }

        if (schema.enumSourceName != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("enumSourceName: '").append(schema.enumSourceName)
                .append("',\n")
        }
        if (schema.enumValues != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("enumValues: [")
            var first = true
            for (enum in schema.enumValues!!) {
                if (first) first = false
                else sb.append(", ")
                sb.append("'").append(enum).append("'")
            }
            sb.append("],\n")
        }

        // TODO Use the format to determine which impl identifier we should use, if any
//        if (schema.implIdentifier != null) {
//            sb.append(" ".repeat(indentLevel * 2)).append("implIdentifier: '").append(schema.implIdentifier)
//                .append("',\n")
//        }

        sb.append(" ".repeat((indentLevel - 1) * 2)).append("} as TypedStringJsonSchema")
    }

    fun outputIntegerSchema(sb: StringBuilder, schema: IntegerJsonSchema, indentLevel: Int) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)

        sb.append(" ".repeat(indentLevel * 2)).append("int64: ").append(schema.int64).append(",\n")
        if (schema.minimum != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("minimum: ").append(schema.minimum).append(",\n")
        }
        if (schema.maximum != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("maximum: ").append(schema.maximum).append(",\n")
        }

        sb.append(" ".repeat((indentLevel - 1) * 2)).append("} as TypedIntegerJsonSchema")
    }

    fun outputFloatSchema(sb: StringBuilder, schema: FloatJsonSchema, indentLevel: Int) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)

        sb.append(" ".repeat(indentLevel * 2)).append("double: ").append(schema.double).append(",\n")
        if (schema.minimum != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("minimum: ").append(schema.minimum).append(",\n")
        }
        if (schema.maximum != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("maximum: ").append(schema.maximum).append(",\n")
        }
        sb.append(" ".repeat((indentLevel - 1) * 2)).append("} as TypedFloatJsonSchema")
    }

    fun outputBooleanSchema(sb: StringBuilder, schema: BooleanJsonSchema, indentLevel: Int) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)
        sb.append(" ".repeat((indentLevel - 1) * 2)).append("}")
    }

    fun outputArraySchema(
        ctx: GeneratorContext,
        sb: StringBuilder,
        parent: ObjectJsonSchema,
        schema: ArrayJsonSchema,
        indentLevel: Int,
    ) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)

        sb.append(" ".repeat(indentLevel * 2)).append("uniqueItems: ").append(schema.uniqueItems).append(",\n")
        sb.append(" ".repeat(indentLevel * 2)).append("nullableItems: ").append(schema.nullableItems).append(",\n")
        if (schema.minItems != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("minItems: ").append(schema.minItems).append(",\n")
        }
        if (schema.maxItems != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("maxItems: ").append(schema.maxItems).append(",\n")
        }

        sb.append(" ".repeat(indentLevel * 2)).append("items: ")
        outputSchema(ctx, sb, parent, schema.items, indentLevel + 1)
        sb.append(",\n")

        sb.append(" ".repeat((indentLevel - 1) * 2)).append("} as TypedArrayJsonSchema<")
            .append(getArrayItemType(ctx, schema.items))
            .append(">")
    }

    private fun getArrayItemType(ctx: GeneratorContext, schema: JsonSchema): String {
        return when (schema) {
            is StringJsonSchema -> "TypedStringJsonSchema"
            is IntegerJsonSchema -> "TypedIntegerJsonSchema"
            is FloatJsonSchema -> "TypedFloatJsonSchema"
            is BooleanJsonSchema -> "TypedBooleanJsonSchema"
            is ArrayJsonSchema -> "TypedArrayJsonSchema<" + getArrayItemType(ctx, schema.items) + ">"
            is ObjectJsonSchema -> "typeof " + ctx.namePrefix + schema.sourceName + "Schema"
            is AnyJsonSchema -> "TypedAnyJsonSchema"
            is RefJsonSchema -> "TypedRefJsonSchema"
        }
    }

    fun outputObjectSchema(ctx: GeneratorContext, sb: StringBuilder, schema: ObjectJsonSchema, indentLevel: Int, parent: ObjectJsonSchema?) {
        // Use already built schemas for nested objects
        if (indentLevel > 1) {
            sb.append(ctx.namePrefix).append(schema.sourceName).append("Schema")
            return
        }

        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)

        sb.append(" ".repeat(indentLevel * 2)).append("sourceName: '").append(schema.sourceName).append("',\n")
            .append(" ".repeat(indentLevel * 2)).append("polymorphicValue: '").append(schema.polymorphicValue).append("',\n")
            .append(" ".repeat(indentLevel * 2)).append("raw: ").append(schema.raw).append(",\n")
        if (schema.minProperties != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("minProperties: ").append(schema.minProperties).append(",\n")
        }
        if (schema.maxProperties != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("maxProperties: ").append(schema.maxProperties).append(",\n")
        }

        sb.append(" ".repeat(indentLevel * 2)).append("properties: {\n")
        // Output parent properties if there is one
        if (parent != null) {
            // TODO This is duplicated code
            for (prop in parent.properties) {
                sb.append(" ".repeat((indentLevel + 1) * 2)).append(prop.name).append(": {\n")
                    .append(" ".repeat((indentLevel + 2) * 2)).append("nullable: ").append(prop.nullable).append(",\n")
                    .append(" ".repeat((indentLevel + 2) * 2)).append("polymorphicKey: ").append(prop.polymorphicKey)
                    .append(",\n")
                    .append(" ".repeat((indentLevel + 2) * 2)).append("name: '").append(prop.name).append("',\n")
                    .append(" ".repeat((indentLevel + 2) * 2)).append("sourceName: '").append(prop.sourceName)
                    .append("',\n")
                if (prop.defaultValue != null) {
                    sb.append(" ".repeat((indentLevel + 2) * 2)).append("defaultValue: ").append(prop.defaultValue)
                        .append(",\n")
                }
                sb.append(" ".repeat((indentLevel + 2) * 2)).append("aliases: [\n")
                for (alias in prop.aliases) {
                    sb.append(" ".repeat((indentLevel + 3) * 2)).append("'").append(alias).append("',\n")
                }
                sb.append(" ".repeat((indentLevel + 2) * 2)).append("],\n")
                    .append(" ".repeat((indentLevel + 2) * 2)).append("schema: ")

                outputSchema(ctx, sb, schema, prop.schema, indentLevel + 3)

                sb.append("\n").append(" ".repeat((indentLevel + 1) * 2)).append("},\n")
            }
        }

        for (prop in schema.properties) {
            sb.append(" ".repeat((indentLevel + 1) * 2)).append(prop.name).append(": {\n")
                .append(" ".repeat((indentLevel + 2) * 2)).append("nullable: ").append(prop.nullable).append(",\n")
                .append(" ".repeat((indentLevel + 2) * 2)).append("polymorphicKey: ").append(prop.polymorphicKey)
                .append(",\n")
                .append(" ".repeat((indentLevel + 2) * 2)).append("name: '").append(prop.name).append("',\n")
                .append(" ".repeat((indentLevel + 2) * 2)).append("sourceName: '").append(prop.sourceName)
                .append("',\n")
            if (prop.defaultValue != null) {
                sb.append(" ".repeat((indentLevel + 2) * 2)).append("defaultValue: ").append(prop.defaultValue)
                    .append(",\n")
            }
            sb.append(" ".repeat((indentLevel + 2) * 2)).append("aliases: [\n")
            for (alias in prop.aliases) {
                sb.append(" ".repeat((indentLevel + 3) * 2)).append("'").append(alias).append("',\n")
            }
            sb.append(" ".repeat((indentLevel + 2) * 2)).append("],\n")
                .append(" ".repeat((indentLevel + 2) * 2)).append("schema: ")

            outputSchema(ctx, sb, schema, prop.schema, indentLevel + 3)

            sb.append("\n").append(" ".repeat((indentLevel + 1) * 2)).append("},\n")
        }
        sb.append(" ".repeat(indentLevel * 2)).append("},\n")

        sb.append(" ".repeat(indentLevel * 2)).append("nullableAdditionalProperties: ")
            .append(schema.nullableAdditionalProperties).append(",\n")
        if (schema.additionalPropertiesKeys != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("additionalPropertiesKeys: ")
            outputSchema(ctx, sb, schema, schema.additionalPropertiesKeys!!, indentLevel + 1)
            sb.append(",\n").append(" ".repeat(indentLevel * 2)).append("additionalPropertiesValues: ")
            outputSchema(ctx, sb, schema, schema.additionalPropertiesValues!!, indentLevel + 1)
            sb.append(",\n")
        }

        if (schema.subTypes != null) {
            sb.append(" ".repeat(indentLevel * 2)).append("subTypes: [\n")
            for (subType in schema.subTypes!!) {
                val resolved = ctx.resolveSubType(schema, subType)

                sb.append(" ".repeat((indentLevel + 1) * 2)).append(ctx.namePrefix).append(resolved.sourceName)
                    .append("Schema")
                    .append(",\n")
            }
            sb.append(" ".repeat(indentLevel * 2)).append("],\n")
        }

        sb.append(" ".repeat((indentLevel - 1) * 2)).append("}")
    }

    fun outputAnySchema(sb: StringBuilder, schema: AnyJsonSchema, indentLevel: Int) {
        sb.append("{\n")
        outputCommonSchema(sb, schema, indentLevel)
        sb.append(" ".repeat((indentLevel - 1) * 2)).append("}")
    }

    fun outputRefSchema(
        ctx: GeneratorContext,
        sb: StringBuilder,
        parent: ObjectJsonSchema,
        schema: RefJsonSchema,
        indentLevel: Int
    ) {
        val resolved = ctx.resolvePropertyRef(schema.identifier, parent)
            ?: throw IllegalStateException("Identifier ${schema.identifier} could not be found")

        outputSchema(ctx, sb, parent, resolved, indentLevel)
    }
}
