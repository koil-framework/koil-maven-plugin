package io.koil.maven.client

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.json.JsonMapper
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.koil.core.config.HttpClientConfig
import io.koil.maven.common.GeneratorContext
import io.koil.schemas.StringJsonSchema
import io.koil.web.client.BaseClient
import io.koil.web.client.ClientContext
import io.koil.web.client.RequestProcessor
import io.koil.web.client.UpstreamCallFailure
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.EndpointContentType
import io.koil.web.webdef.RequestParameterType
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpMethod
import io.vertx.core.streams.ReadStream
import java.net.ConnectException
import java.net.URLEncoder

fun generateClient(
    ctx: GeneratorContext,
    apiDefinitionClass: ClassName,
    endpoints: List<Endpoint>
): FileSpec {
    val typeSpec = TypeSpec.classBuilder(ctx.clientClassName(apiDefinitionClass.simpleName))
        .superclass(BaseClient::class)
        .addSuperclassConstructorParameter("upstreamServiceName")
        .addSuperclassConstructorParameter("vertx")
        .addSuperclassConstructorParameter("config")
        .addSuperclassConstructorParameter("options")
        .addSuperclassConstructorParameter("jsonMapper")
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameter(ParameterSpec("upstreamServiceName", String::class.asTypeName()))
                .addParameter(ParameterSpec("vertx", Vertx::class.asTypeName()))
                .addParameter(ParameterSpec("config", HttpClientConfig::class.asTypeName()))
                .addParameter(ParameterSpec("options", HttpClientOptions::class.asTypeName()))
                .addParameter(ParameterSpec("jsonMapper", JsonMapper::class.asTypeName()))
                .addParameter(ParameterSpec("requestProcessors", Set::class.parameterizedBy(RequestProcessor::class)))
                .build()
        ).addProperty(
            PropertySpec.builder(
                "requestProcessors",
                Set::class.parameterizedBy(RequestProcessor::class),
                KModifier.PRIVATE
            )
                .initializer("requestProcessors")
                .build()
        )

    for (endpoint in endpoints) {
        if (endpoint.hidden) continue

        val funSpec = FunSpec.builder(endpoint.methodName)
            .addParameter("context", ClientContext::class)
            .addKdoc(endpoint.description) // TODO Improve description format and content
            .returns(getReturnedType(ctx, endpoint))

        val code = CodeBlock.builder()
            .addStatement("val operationId = %S", endpoint.operationId)
            .add("val opts = createRequestOptions(context)\n")
            .indent()
            .add(".setMethod(%T.%L)\n", HttpMethod::class, endpoint.method.name())

        // Params
        if (endpoint.requestBody.type != EndpointContentType.NONE) {
            val paramType = if (endpoint.requestBody.stream) {
                ReadStream::class.parameterizedBy(Buffer::class)
            } else if (endpoint.requestBody.schema != null) {
                ctx.resolveType(endpoint.requestBody.schema!!)
            } else {
                when (endpoint.requestBody.type) {
                    EndpointContentType.JSON -> JsonNode::class.asTypeName()
                    EndpointContentType.BINARY -> Buffer::class.asTypeName()
                    EndpointContentType.TEXT -> String::class.asTypeName()
                    EndpointContentType.NONE -> throw IllegalStateException("This cannot happen")
                }
            }

            funSpec.addParameter("requestBody", paramType)
            code.add(".addHeader(\"Content-Type\", %S)\n", endpoint.requestBody.expectedContentType)
        }

        code.unindent()
            .add("\n")
            .addStatement("val queryParams = mutableListOf<String>()")

        var resolvedPath = endpoint.path
        var requiredParams = false
        for (param in endpoint.params) {
            val optional = !param.required
            if (optional && param.type != RequestParameterType.PATH) code.add("if (%L != null) ", param.name)

            when (param.type) {
                RequestParameterType.PATH -> resolvedPath = resolvedPath.replace(":${param.name}", "\$${param.name}")
                RequestParameterType.QUERY -> {
                    requiredParams = requiredParams || param.required

                    // TODO Handle arrays/objects
                    val schema = param.schema
                    if (schema is StringJsonSchema) {
                        val nameSuffix =
                            if (schema.implIdentifier != null) ".toString()"
                            // This works because we accept case-insensitive enums for now
                            // TODO We should keep track of whether lowercase enum is enabled
                            // TODO Support case insensitive enums
                            else if (schema.enumSourceName != null) ".name.lowercase()"
                            else ""

                        code.add(
                            "queryParams.add(\"%L=\" + %T.encode(%L%L, %T.UTF_8))\n",
                            param.name,
                            URLEncoder::class,
                            param.name,
                            nameSuffix,
                            Charsets::class
                        )
                    } else {
                        code.add("queryParams.add(\"%L=\$%L\")\n", param.name, param.name)
                    }
                }

                RequestParameterType.HEADER -> code.add("opts.addHeader(%S, %L)\n", param.name, param.name)
                // TODO Cookie param type
                else -> {}
            }
            funSpec.addParameter(
                ParameterSpec.builder(param.name, ctx.resolveType(param.schema).copy(nullable = optional))
                    .addKdoc(param.description)
                    .build()
            )
        }

        // Small optimisation to remove the condition if we're sure the list has, by design, at least one element
        if (requiredParams) code.addStatement("val queryString = queryParams.joinToString(\"&\", \"?\")")
        else {
            code.addStatement("val queryString = if (queryParams.isEmpty()) \"\" else queryParams.joinToString(\"&\", \"?\")")
        }

        val completePath = "/${endpoint.namespace}/v${endpoint.apiVersion}$resolvedPath\$queryString"

        code.addStatement("opts.setURI(\"%L\")", completePath)
            .add("\n")

            .beginControlFlow("for (processor in requestProcessors)")
            .addStatement("processor.process(context, opts)")
            .endControlFlow()
            .add("\n")

            .addStatement("return httpClient.request(opts)")
            .indent()

        if (endpoint.requestBody.type == EndpointContentType.NONE) {
            code.add(".compose(%T::send)\n", HttpClientRequest::class)
        } else if (endpoint.requestBody.stream) code.add(".compose { it.send(requestBody.resume()) }\n")
        else if (endpoint.requestBody.type == EndpointContentType.JSON) {
            code.add(
                ".compose { it.send(jsonMapper.%M(requestBody)) }\n",
                MemberName("io.koil.core", "writeValueAsBuffer")
            )
        } else code.add(".compose { it.send(requestBody) }\n")

        code.add(".transform {\n")
            .indent()
            .beginControlFlow("if (it.failed())")
            .addStatement("val cause = it.cause()")
            .beginControlFlow("if (cause is %T)", ConnectException::class)
            .addStatement(
                "%T.failedFuture(%T.Network(upstreamServiceName, operationId, cause))",
                Future::class,
                UpstreamCallFailure::class
            )
            .nextControlFlow("else")
            .addStatement("%T.failedFuture(cause)", Future::class)
            .endControlFlow()
            .nextControlFlow("else")
            .addStatement("val res = it.result()")
            .beginControlFlow("if (res.statusCode() == %L)", endpoint.mainResponse.status)

        if (endpoint.mainResponse.stream) {
            code.addStatement("res.pause()")
                .addStatement("%T.succeededFuture(res)", Future::class)
        } else if (endpoint.noResponseProcessing) {
            code.addStatement("res.body()")
        } else if (endpoint.mainResponse.schema == null) {
            code.addStatement("%T.succeededFuture(Unit)", Future::class)
        } else {
            code.addStatement(
                "responseBodyHandler(operationId, res, %T::class.java)",
                ctx.resolveType(endpoint.mainResponse.schema!!)
            )
        }

        code.nextControlFlow("else")
            .addStatement("handleError(operationId, res)")
            .endControlFlow()
            .endControlFlow()
            .unindent()
            .add("}\n")
            .unindent()

        typeSpec.addFunction(funSpec.addCode(code.build()).build())
    }

    return FileSpec.builder(ctx.clientClassName(apiDefinitionClass.simpleName))
        .addType(typeSpec.build())
        .build()
}

private fun getReturnedType(ctx: GeneratorContext, endpoint: Endpoint): TypeName {
    val type =
        if (endpoint.mainResponse.stream) ReadStream::class.asTypeName().parameterizedBy(Buffer::class.asTypeName())
        else if (endpoint.noResponseProcessing) Buffer::class.asTypeName()
        else if (endpoint.mainResponse.schema != null) ctx.resolveType(endpoint.mainResponse.schema!!)
        else Unit::class.asTypeName()

    return Future::class.asTypeName().parameterizedBy(type)
}
