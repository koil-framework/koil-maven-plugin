package io.koil.maven.mojos

import com.squareup.kotlinpoet.ClassName
import io.koil.maven.client.generateClient
import io.koil.maven.common.*
import io.koil.maven.server.generateController
import io.koil.maven.server.generateRouter
import io.koil.web.webdef.ApiDefinition
import io.koil.web.webdef.Endpoint
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.plugins.annotations.ResolutionScope
import org.apache.maven.project.MavenProject
import java.io.File
import java.net.URLClassLoader
import kotlin.io.path.Path

class ApiConfig {
    @Parameter(name = "apiClass")
    var apiClass: String = ""

    @Parameter(name = "basePackage")
    var basePackage: String = ""

    @Parameter(name = "mode", defaultValue = "server")
    var mode: String = "server"

    @Parameter(name = "modelPrefix", defaultValue = "")
    var modelPrefix: String = ""
}


@Mojo(
    name = "generate",
    defaultPhase = LifecyclePhase.GENERATE_SOURCES,
    requiresDependencyResolution = ResolutionScope.COMPILE,
    requiresDependencyCollection = ResolutionScope.COMPILE,
)
class GeneratorMojo : AbstractMojo() {
    @Parameter(name = "apis")
    var apis: MutableList<ApiConfig> = mutableListOf()

    @Parameter(readonly = true, defaultValue = "\${project}")
    private val project: MavenProject? = null

    private fun getClassLoader(project: MavenProject): ClassLoader {
        return try {
            val array = project.compileClasspathElements.map { File(it).toURI().toURL() }.toTypedArray()
            URLClassLoader(array, this.javaClass.classLoader)
        } catch (e: Exception) {
            throw MojoFailureException("Failed to load project classes", e)
        }
    }

    override fun execute() {
        if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        val classLoader = getClassLoader(project)

        for (apiConfig in apis) {
            runExecution(classLoader, apiConfig)
        }
    }

    private fun runExecution(classLoader: ClassLoader, apiConfig: ApiConfig) {
        if (apiConfig.apiClass.isBlank()) {
            throw MojoFailureException("No API definition class was defined")
        } else if (apiConfig.basePackage.isBlank()) {
            throw MojoFailureException("No base package was defined")
        } else if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        log.info("Running generation for ${apiConfig.apiClass}")

        val apiDefinition = try {
            loadApiDefinitionInstance(classLoader, apiConfig)
        } catch (e: Exception) {
            throw MojoExecutionException("Failed to load API definition class", e)
        }

        // Group endpoints by controllers, extracting objects to make a registry
        val controllersEndpoints = mutableMapOf<String, MutableList<Endpoint>>()
        val schemaRegistry = SchemaRegistry()
        val ctx = GeneratorContext(schemaRegistry, apiConfig.basePackage, apiConfig.modelPrefix)
        val endpoints = apiDefinition.endpoints()
        for (endpoint in endpoints) {
            ctx.register(endpoint)
            controllersEndpoints.computeIfAbsent(endpoint.controllerName) { mutableListOf() }
                .add(endpoint)
        }

        val schemasWithIdentifiers = apiDefinition.schemasWithIdentifiers()
        for (schema in schemasWithIdentifiers) {
            schemaRegistry.registerWithIdentifier(schema.key, schema.value)
        }

        // Generate objects sources
        log.info("Generating ${schemaRegistry.size} model objects")
        for (obj in schemaRegistry.registeredSchemas) {
            if (obj.written) continue
            else obj.written = true

            when (obj) {
                is SchemaTypeInfo.Obj -> generateObject(ctx, obj.schema)
                is SchemaTypeInfo.Enum -> generateEnum(ctx, obj.schema)
                else -> null
            }?.writeTo(Path(project.build.directory + "/generated-sources/koil"))
        }

        if (apiConfig.mode == "server") {
            for (controllerEndpoints in controllersEndpoints.values) {
                log.info("Generating controller ${controllerEndpoints[0].controllerName}")
                generateController(ctx, controllerEndpoints)
                    .writeTo(Path(project.build.directory + "/generated-sources/koil"))
            }

            log.info("Generating router")
            generateRouter(ctx, ClassName.bestGuess(apiConfig.apiClass), endpoints)
                .writeTo(Path(project.build.directory + "/generated-sources/koil"))
        } else {
            log.info("Generating API client")
            generateClient(ctx, ClassName.bestGuess(apiConfig.apiClass), endpoints)
                .writeTo(Path(project.build.directory + "/generated-sources/koil"))
        }
    }

    private fun loadApiDefinitionInstance(classLoader: ClassLoader, apiConfig: ApiConfig): ApiDefinition {
        val cls = classLoader.loadClass(apiConfig.apiClass)
        val ctor = cls.getConstructor()
        return ctor.newInstance() as ApiDefinition
    }
}
