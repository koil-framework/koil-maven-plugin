package io.koil.maven.mojos

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.koil.core.KoilJacksonModule
import io.koil.schemas.*
import io.koil.schemas.JsonSchema
import io.koil.web.Problem
import io.koil.web.webdef.ApiDefinition
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.RequestParameter
import io.koil.web.webdef.RequestParameterType
import io.swagger.v3.core.jackson.ExampleSerializer
import io.swagger.v3.core.jackson.MediaTypeSerializer
import io.swagger.v3.core.jackson.SchemaSerializer
import io.swagger.v3.core.jackson.mixin.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.callbacks.Callback
import io.swagger.v3.oas.models.examples.Example
import io.swagger.v3.oas.models.headers.Header
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import io.swagger.v3.oas.models.links.Link
import io.swagger.v3.oas.models.links.LinkParameter
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.RequestBody
import io.swagger.v3.oas.models.responses.ApiResponse
import io.swagger.v3.oas.models.responses.ApiResponses
import io.swagger.v3.oas.models.security.OAuthFlow
import io.swagger.v3.oas.models.security.OAuthFlows
import io.swagger.v3.oas.models.security.Scopes
import io.swagger.v3.oas.models.security.SecurityScheme
import io.swagger.v3.oas.models.servers.Server
import io.swagger.v3.oas.models.servers.ServerVariable
import io.swagger.v3.oas.models.servers.ServerVariables
import io.swagger.v3.oas.models.tags.Tag
import io.vertx.core.http.HttpMethod
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.plugins.annotations.ResolutionScope
import org.apache.maven.project.MavenProject
import java.io.File
import java.math.BigDecimal
import java.net.URLClassLoader
import io.swagger.v3.oas.models.parameters.Parameter as OpenApiParameter

class OpenApiConfig {
    @Parameter(name = "apiClass")
    var apiClass: String = ""
}

class SwaggerModule : SimpleModule() {
    override fun setupModule(context: SetupContext) {
        super.setupModule(context)
        context.addBeanSerializerModifier(object : BeanSerializerModifier() {
            override fun modifySerializer(
                config: SerializationConfig,
                beanDesc: BeanDescription,
                serializer: JsonSerializer<*>,
            ): JsonSerializer<*> {
                if (Schema::class.java.isAssignableFrom(beanDesc.beanClass)) {
                    return SchemaSerializer(serializer as JsonSerializer<Any?>)
                } else if (MediaType::class.java.isAssignableFrom(beanDesc.beanClass)) {
                    return MediaTypeSerializer(serializer as JsonSerializer<Any?>)
                } else if (Example::class.java.isAssignableFrom(beanDesc.beanClass)) {
                    return ExampleSerializer(serializer as JsonSerializer<Any?>)
                }
                return serializer
            }
        })
    }
}

@Mojo(
    name = "generate-openapi",
    defaultPhase = LifecyclePhase.PROCESS_CLASSES,
    requiresDependencyResolution = ResolutionScope.COMPILE,
    requiresDependencyCollection = ResolutionScope.COMPILE,
)
class OpenApiGeneratorMojo : AbstractMojo() {
    @Parameter(name = "apis")
    var apis: MutableList<OpenApiConfig> = mutableListOf()

    @Parameter(readonly = true, defaultValue = "\${project}")
    private val project: MavenProject? = null

    private fun getClassLoader(project: MavenProject): ClassLoader {
        return try {
            val classpathElements = mutableListOf<String>()
            classpathElements.addAll(project.compileClasspathElements)
            classpathElements.add(project.build.outputDirectory)

            val array = classpathElements.map { File(it).toURI().toURL() }.toTypedArray()
            URLClassLoader(array, this.javaClass.classLoader)
        } catch (e: Exception) {
            throw MojoFailureException("Failed to load project classes", e)
        }
    }

    private fun prepareMapper(builder: JsonMapper.Builder): JsonMapper.Builder = builder
        .addModule(KoilJacksonModule())
        .addModule(KotlinModule.Builder().build())
        .addModule(JavaTimeModule())
        .addModule(SwaggerModule())
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
        .addMixIn(ApiResponses::class.java, ExtensionsMixin::class.java)
        .addMixIn(Contact::class.java, ExtensionsMixin::class.java)
        .addMixIn(Encoding::class.java, ExtensionsMixin::class.java)
        .addMixIn(EncodingProperty::class.java, ExtensionsMixin::class.java)
        .addMixIn(Example::class.java, ExampleMixin::class.java)
        .addMixIn(ExternalDocumentation::class.java, ExtensionsMixin::class.java)
        .addMixIn(Link::class.java, ExtensionsMixin::class.java)
        .addMixIn(LinkParameter::class.java, ExtensionsMixin::class.java)
        .addMixIn(MediaType::class.java, MediaTypeMixin::class.java)
        .addMixIn(OAuthFlow::class.java, ExtensionsMixin::class.java)
        .addMixIn(OAuthFlows::class.java, ExtensionsMixin::class.java)
        .addMixIn(Operation::class.java, OperationMixin::class.java)
        .addMixIn(PathItem::class.java, ExtensionsMixin::class.java)
        .addMixIn(Paths::class.java, ExtensionsMixin::class.java)
        .addMixIn(Scopes::class.java, ExtensionsMixin::class.java)
        .addMixIn(Server::class.java, ExtensionsMixin::class.java)
        .addMixIn(ServerVariable::class.java, ExtensionsMixin::class.java)
        .addMixIn(ServerVariables::class.java, ExtensionsMixin::class.java)
        .addMixIn(Tag::class.java, ExtensionsMixin::class.java)
        .addMixIn(XML::class.java, ExtensionsMixin::class.java)
        .addMixIn(ApiResponse::class.java, ExtensionsMixin::class.java)
        .addMixIn(io.swagger.v3.oas.models.parameters.Parameter::class.java, ExtensionsMixin::class.java)
        .addMixIn(RequestBody::class.java, ExtensionsMixin::class.java)
        .addMixIn(Header::class.java, ExtensionsMixin::class.java)
        .addMixIn(SecurityScheme::class.java, ExtensionsMixin::class.java)
        .addMixIn(Callback::class.java, ExtensionsMixin::class.java)
        .addMixIn(Schema::class.java, SchemaMixin::class.java)
        .addMixIn(DateSchema::class.java, DateSchemaMixin::class.java)
        .addMixIn(Components::class.java, ComponentsMixin::class.java)
        .addMixIn(Info::class.java, InfoMixin::class.java)
        .addMixIn(License::class.java, LicenseMixin::class.java)
        .addMixIn(OpenAPI::class.java, OpenAPIMixin::class.java)
        .addMixIn(Discriminator::class.java, DiscriminatorMixin::class.java)

    override fun execute() {
        if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        val classLoader = getClassLoader(project)

        val mapper = prepareMapper(JsonMapper.builder()).build()

        val yamlFactory = YAMLFactory()
        yamlFactory.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
        yamlFactory.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
        yamlFactory.enable(YAMLGenerator.Feature.SPLIT_LINES)
        yamlFactory.enable(YAMLGenerator.Feature.ALWAYS_QUOTE_NUMBERS_AS_STRINGS)

        val yamlMapper = prepareMapper(JsonMapper.builder(yamlFactory)).build()

        for (openApiConfig in apis) {
            generateOpenApiDefinition(mapper, yamlMapper, classLoader, openApiConfig)
        }
    }

    private fun generateOpenApiDefinition(
        mapper: JsonMapper,
        yamlMapper: JsonMapper,
        classLoader: ClassLoader,
        openApiConfig: OpenApiConfig,
    ) {
        if (openApiConfig.apiClass.isBlank()) {
            throw MojoFailureException("No API definition class was defined")
        } else if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        log.info("Generating OpenApi definition for ${openApiConfig.apiClass}")

        val apiDefinition = try {
            loadApiDefinitionInstance(classLoader, openApiConfig.apiClass)
        } catch (e: Exception) {
            throw MojoExecutionException("Failed to load API definition class", e)
        }

        val openApi = OpenAPI(SpecVersion.V30)
            .info(
                Info()
                    .title(project.name)
                    .description(project.description)
                    .version(project.version)
            ).components(
                Components()
                    .addSchemas("Problem", convertSchema(Problem.schema, false, null))
                    .addResponses(
                        "NotFound",
                        ApiResponse()
                            .description("The requested resource was not found")
                            .content(
                                Content().addMediaType(
                                    "application/json",
                                    MediaType()
                                        .schema(Schema<Any>().`$ref`("#/components/schemas/Problem"))
                                        .example(Problem.notFound("events", "JyLZCeCvT_uAMaGJyXgbTA").build())
                                )
                            )
                    )
                    .addResponses(
                        "Unauthorized",
                        ApiResponse()
                            .description("Authentication is required. Re authenticating is likely required")
                            .content(
                                Content().addMediaType(
                                    "application/json",
                                    MediaType()
                                        .schema(Schema<Any>().`$ref`("#/components/schemas/Problem"))
                                        .addExamples(
                                            "Authentication required",
                                            Example()
                                                .summary("Authentication is required")
                                                .value(Problem.authenticationRequired().build())
                                        ).addExamples(
                                            "Invalid token",
                                            Example()
                                                .summary("The provided access token is invalid")
                                                .value(Problem.invalidAccessToken().build())
                                        ).addExamples(
                                            "Expired token",
                                            Example()
                                                .summary("The provided access token is expired")
                                                .value(Problem.accessTokenExpired().build())
                                        ).addExamples(
                                            "Revoked token",
                                            Example()
                                                .summary("The provided access token is revoked")
                                                .value(Problem.accessTokenRevoked().build())
                                        )
                                )
                            )
                    )

                    .addResponses(
                        "Forbidden",
                        ApiResponse()
                            .description("The permissions requirements are not met")
                            .content(
                                Content().addMediaType(
                                    "application/json",
                                    MediaType()
                                        .schema(Schema<Any>().`$ref`("#/components/schemas/Problem"))
                                        .addExamples(
                                            "Account disabled",
                                            Example()
                                                .summary("The account is disabled and cannot be used")
                                                .value(Problem.accountDisabled().build())
                                        ).addExamples(
                                            "Not enough permissions",
                                            Example()
                                                .summary("The required permissions are not met")
                                                .value(
                                                    Problem.notEnoughPermissions("products", "write", "read").build()
                                                )
                                        ).addExamples(
                                            "No permissions",
                                            Example()
                                                .summary("The user doesn't have any permission")
                                                .value(Problem.noPermissions().build())
                                        )
                                )
                            )
                    )
                    .addResponses(
                        "InternalError",
                        ApiResponse()
                            .description("An internal error occurred")
                            .content(
                                Content().addMediaType(
                                    "application/json",
                                    MediaType()
                                        .schema(Schema<Any>().`$ref`("#/components/schemas/Problem"))
                                        .example(Problem.internalError().build())
                                )
                            )
                    )
            )

        registerEndpoints(openApi, apiDefinition.endpoints())

        File("${project.build.directory}/${apiDefinition::class.simpleName}-openapi.json")
            .writeText(mapper.writeValueAsString(openApi))
        File("${project.build.directory}/${apiDefinition::class.simpleName}-openapi.yaml")
            .writeText(yamlMapper.writer(DefaultPrettyPrinter()).writeValueAsString(openApi))
    }

    private fun loadApiDefinitionInstance(classLoader: ClassLoader, apiClass: String): ApiDefinition {
        val cls = classLoader.loadClass(apiClass)
        val ctor = cls.getConstructor()
        return ctor.newInstance() as ApiDefinition
    }

    private fun registerEndpoints(openApi: OpenAPI, endpoints: List<Endpoint>) {
        // <path, endpoints>>
        val endpointPaths = mutableMapOf<String, MutableList<Endpoint>>()
        val tags = mutableSetOf<String>()

        for (endpoint in endpoints) {
            if (endpoint.hidden) continue

            tags.addAll(endpoint.tags)

            // Replace any path param
            val paramNames = endpoint.params.filter { it.type == RequestParameterType.PATH }.map { it.name }

            var path = endpoint.path
            for (paramName in paramNames) {
                path = path.replace(":$paramName", "{$paramName}")
            }

            // Group endpoints by path for the open api spec
            endpointPaths.computeIfAbsent("/${endpoint.namespace}/v${endpoint.apiVersion}$path") { mutableListOf() }
                .add(endpoint)
        }

        // TODO Add tag descriptions
        openApi.tags(tags.map { Tag().name(it) })

        // Create open api path items
        for ((path, eps) in endpointPaths) {
            val items = PathItem()

            for (ep in eps) {
                val op = endpointToOperation(ep)
                when (ep.method) {
                    HttpMethod.GET -> items.get(op)
                    HttpMethod.POST -> items.post(op)
                    HttpMethod.PUT -> items.put(op)
                    HttpMethod.PATCH -> items.patch(op)
                    HttpMethod.DELETE -> items.delete(op)
                    HttpMethod.HEAD -> items.head(op)
                    HttpMethod.OPTIONS -> items.options(op)
                    HttpMethod.TRACE -> items.trace(op)
                }
            }

            openApi.path(path, items)
        }
    }
}

private fun endpointToOperation(endpoint: Endpoint): Operation {
    // TODO Add permissions documentation to description
    val op = Operation()
        .summary(endpoint.summary)
        .description(endpoint.description)
        .tags(endpoint.tags.toList())
        .operationId(endpoint.operationId)
        .deprecated(endpoint.deprecated)

    // Params
    for (param in endpoint.params) {
        op.addParametersItem(convertParameter(param))
    }

    // Request body
    // TODO Take other types of request body into account
    if (endpoint.requestBody.schema != null) {
        op.requestBody(
            RequestBody()
                .required(true)
                .content(
                    Content()
                        .addMediaType(
                            "application/json",
                            MediaType()
                                .schema(convertSchema(endpoint.requestBody.schema!!, false, null))
                        )
                )
        )
    }

    // Responses
    val responses = ApiResponses()

    val mainResponse = ApiResponse()
        .description(endpoint.mainResponse.description)
    if (endpoint.mainResponse.schema != null) {
        mainResponse.content(
            Content().addMediaType(
                "application/json",
                MediaType().schema(convertSchema(endpoint.mainResponse.schema!!, false, null))
            )
        )
    }
    responses.addApiResponse(endpoint.mainResponse.status.toString(), mainResponse)

    if (endpoint.requiresAuthentication) {
        responses.addApiResponse("401", ApiResponse().`$ref`("#/components/responses/Unauthorized"))
            .addApiResponse("403", ApiResponse().`$ref`("#/components/responses/Forbidden"))
    }

    for (res in endpoint.errorResponses) {
        if (res.status == 404) {
            responses.addApiResponse("404", ApiResponse().`$ref`("#/components/responses/NotFound"))
            continue
        }

        val r = ApiResponse()
            .description(res.description)

        val resSchema = res.schema
        if (resSchema is ObjectJsonSchema) {
            // TODO Hardcoded ref
            val mediaType = if (resSchema.sourceName == "Problem") {
                MediaType().schema(ObjectSchema().`$ref`("#/components/schemas/Problem"))
            } else {
                MediaType().schema(convertSchema(resSchema, false, null))
            }

            mediaType.example(res.example)
            r.content(Content().addMediaType("application/json", mediaType))
        }

        responses.addApiResponse(res.status.toString(), r)
    }
    responses.addApiResponse("500", ApiResponse().`$ref`("#/components/responses/InternalError"))

    return op.responses(responses)
}

private fun convertParameter(param: RequestParameter<*>): OpenApiParameter = OpenApiParameter()
    .`in`(param.type.name.lowercase())
    .name(param.name)
    .description(param.description)
    .required(param.required)
    .example(param.example)
    .schema(convertSchema(param.schema, !param.required, null)) // TODO Add param default value

private fun convertSchema(schema: JsonSchema, nullable: Boolean, defaultValue: JsonNode?): Schema<*> {
    return when (schema) {
        is StringJsonSchema -> convertStringSchema(schema, nullable, defaultValue)
        is IntegerJsonSchema -> convertIntSchema(schema, nullable, defaultValue)
        is FloatJsonSchema -> convertFloatSchema(schema, nullable, defaultValue)
        is BooleanJsonSchema -> convertBoolSchema(schema, nullable, defaultValue)
        is ArrayJsonSchema -> convertArraySchema(schema, nullable)
        is ObjectJsonSchema -> convertObjectSchema(schema, nullable)
        is AnyJsonSchema -> convertAnySchema(schema, nullable)
        is RefJsonSchema -> convertRefSchema(schema)
    }
}

private fun convertStringSchema(schema: StringJsonSchema, nullable: Boolean, defaultValue: JsonNode?): Schema<*> =
    StringSchema()
        .description(schema.description)
//        .example(schema.example)
        .nullable(nullable)
        .format(schema.format)
        ._default(defaultValue?.textValue())
        ._const(schema.const)
        .pattern(schema.pattern)
        .minLength(schema.minLength)
        .maxLength(schema.maxLength)
        ._enum(schema.enumValues?.toList())

private fun convertIntSchema(schema: IntegerJsonSchema, nullable: Boolean, defaultValue: JsonNode?): Schema<*> =
    IntegerSchema()
        .description(schema.description)
//    .example(schema.example)
        .nullable(nullable)
        .minimum(schema.minimum?.let(BigDecimal::valueOf))
        .maximum(schema.maximum?.let(BigDecimal::valueOf))
        .format(if (schema.int64) "int64" else "int32")
        ._default(defaultValue?.textValue())

private fun convertFloatSchema(schema: FloatJsonSchema, nullable: Boolean, defaultValue: JsonNode?): Schema<*> =
    IntegerSchema()
        .description(schema.description)
//    .example(schema.example)
        .nullable(nullable)
        .minimum(schema.minimum?.let(BigDecimal::valueOf))
        .maximum(schema.maximum?.let(BigDecimal::valueOf))
        .format(if (schema.double) "double" else "float")
        ._default(defaultValue?.textValue())

private fun convertBoolSchema(schema: BooleanJsonSchema, nullable: Boolean, defaultValue: JsonNode?): Schema<*> =
    BooleanSchema()
        .description(schema.description)
//    .example(schema.example?.textValue())
        .nullable(nullable)
        ._default(defaultValue?.textValue())

private fun convertArraySchema(schema: ArrayJsonSchema, nullable: Boolean): Schema<*> = ArraySchema()
    .description(schema.description)
//    .example(schema.example)
    .nullable(nullable)
    .minItems(schema.minItems)
    .maxItems(schema.maxItems)
    .uniqueItems(schema.uniqueItems)
    .items(convertSchema(schema.items, schema.nullableItems, null))

private fun convertAnySchema(schema: AnyJsonSchema, nullable: Boolean): Schema<*> = ObjectSchema()
    .description(schema.description)
//    .example(schema.example)
    .nullable(nullable)

private fun convertRefSchema(schema: RefJsonSchema): Schema<*> = ObjectSchema()
    .description(schema.description)
    .`$ref`("#/components/schemas/${schema.identifier}")

private fun convertObjectSchema(schema: ObjectJsonSchema, nullable: Boolean): Schema<*> {
    val openApiSchema = ObjectSchema()
        .description(schema.description)
//        .example(schema.example)
        .nullable(nullable)
        .minProperties(schema.minProperties)
        .maxProperties(schema.maxProperties)

    if (schema.raw) {
        openApiSchema.additionalProperties(true)
        return openApiSchema
    } else {
        openApiSchema.title(schema.sourceName)
    }

    for (property in schema.properties) {
        openApiSchema.addProperty(
            property.name,
            convertSchema(property.schema, property.nullable, property.defaultValue)
        )
    }

    if (schema.additionalPropertiesKeys != null && schema.additionalPropertiesValues != null) {
        openApiSchema
            .propertyNames(convertSchema(schema.additionalPropertiesKeys!!, false, null))
            .additionalProperties(
                convertSchema(
                    schema.additionalPropertiesValues!!,
                    schema.nullableAdditionalProperties,
                    null
                )
            )
    }

    return openApiSchema
}
