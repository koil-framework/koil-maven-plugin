package io.koil.maven.mojos

import com.squareup.kotlinpoet.ClassName
import io.koil.maven.client.generateJsClient
import io.koil.maven.common.GeneratorContext
import io.koil.maven.common.SchemaRegistry
import io.koil.schemas.ObjectJsonSchema
import io.koil.web.webdef.ApiDefinition
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.plugins.annotations.ResolutionScope
import org.apache.maven.project.MavenProject
import java.io.File
import java.net.URLClassLoader

class JsClientConfig {
    @Parameter(name = "apiClass")
    var apiClass: String = ""

    @Parameter(name = "modelPrefix", defaultValue = "")
    var modelPrefix: String = ""

    @Parameter(name = "fileName")
    var fileName: String = ""
}

@Mojo(
    name = "generate-js-client",
    defaultPhase = LifecyclePhase.GENERATE_SOURCES,
    requiresDependencyResolution = ResolutionScope.COMPILE,
    requiresDependencyCollection = ResolutionScope.COMPILE,
)
class JsClientGeneratorMojo : AbstractMojo() {
    @Parameter(name = "jsClients")
    var jsClients: MutableList<JsClientConfig> = mutableListOf()

    @Parameter(name = "clientOutputDir", defaultValue = "target/")
    var clientOutputDir: String = "target/"

    @Parameter(readonly = true, defaultValue = "\${project}")
    private val project: MavenProject? = null

    private fun getClassLoader(project: MavenProject): ClassLoader {
        return try {
            val array = project.compileClasspathElements.map { File(it).toURI().toURL() }.toTypedArray()
            URLClassLoader(array, this.javaClass.classLoader)
        } catch (e: Exception) {
            throw MojoFailureException("Failed to load project classes", e)
        }
    }

    override fun execute() {
        if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        val classLoader = getClassLoader(project)

        val outDir = if (clientOutputDir.endsWith("/")) clientOutputDir else "$clientOutputDir/"

        for (apiConfig in jsClients) {
            File(outDir + "${apiConfig.fileName}.ts").writeText(runExecution(classLoader, apiConfig))
        }
    }

    private fun runExecution(classLoader: ClassLoader, apiConfig: JsClientConfig): String {
        if (apiConfig.apiClass.isBlank()) {
            throw MojoFailureException("No API definition class was defined")
        } else if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        log.info("Running JS client generation for ${apiConfig.apiClass}")

        val apiDefinition = try {
            loadApiDefinitionInstance(classLoader, apiConfig)
        } catch (e: Exception) {
            throw MojoExecutionException("Failed to load API definition class", e)
        }

        // Group endpoints by controllers, extracting objects to make a registry
        val objectRegistry = mutableMapOf<String, ObjectJsonSchema>()
        val schemaRegistry = SchemaRegistry()
        val ctx = GeneratorContext(schemaRegistry, "", apiConfig.modelPrefix)
        val endpoints = apiDefinition.endpoints()
        for (endpoint in endpoints) {
            ctx.register(endpoint)
        }

        val schemasWithIdentifiers = apiDefinition.schemasWithIdentifiers()
        for (schema in schemasWithIdentifiers) {
            schemaRegistry.registerWithIdentifier(schema.key, schema.value)
        }

        // Generate objects sources
        log.info("Generating ${objectRegistry.size} model objects")

        return generateJsClient(ClassName.bestGuess(apiConfig.apiClass), ctx, endpoints)
    }

    private fun loadApiDefinitionInstance(classLoader: ClassLoader, apiConfig: JsClientConfig): ApiDefinition {
        val cls = classLoader.loadClass(apiConfig.apiClass)
        val ctor = cls.getConstructor()
        return ctor.newInstance() as ApiDefinition
    }
}
