package io.koil.maven.mojos

import io.koil.db.*
import io.koil.maven.db.generateDbModel
import io.koil.maven.db.generateRepository
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.plugins.annotations.ResolutionScope
import org.apache.maven.project.MavenProject
import java.io.File
import java.net.URLClassLoader
import kotlin.io.path.Path

class RepositoryConfig {
    @Parameter(name = "definitionClass")
    var definitionClass: String = ""

    @Parameter(name = "basePackage")
    var basePackage: String = ""
}

@Mojo(
    name = "generate-repositories",
    defaultPhase = LifecyclePhase.GENERATE_SOURCES,
    requiresDependencyResolution = ResolutionScope.COMPILE,
    requiresDependencyCollection = ResolutionScope.COMPILE,
)
class DbGeneratorMojo : AbstractMojo() {
    @Parameter(name = "repositories")
    var repositories: MutableList<RepositoryConfig> = mutableListOf()

    @Parameter(readonly = true, defaultValue = "\${project}")
    private val project: MavenProject? = null

    private fun getClassLoader(project: MavenProject): ClassLoader {
        return try {
            val array = project.compileClasspathElements.map { File(it).toURI().toURL() }.toTypedArray()
            URLClassLoader(array, this.javaClass.classLoader)
        } catch (e: Exception) {
            throw MojoFailureException("Failed to load project classes", e)
        }
    }

    override fun execute() {
        if (project == null) throw MojoExecutionException("Failed to get maven project")
        val classLoader = getClassLoader(project)

        for (repository in repositories) {
            runExecution(classLoader, repository)
        }
    }

    private fun runExecution(classLoader: ClassLoader, repository: RepositoryConfig) {
        if (repository.definitionClass.isBlank()) {
            throw MojoFailureException("No repository definition class was defined")
        } else if (repository.basePackage.isBlank()) {
            throw MojoFailureException("No base package was defined")
        } else if (project == null) {
            throw MojoExecutionException("Failed to get maven project")
        }

        log.info("Running generation for ${repository.definitionClass}")

        val repositoryDefinition = try {
            loadRepositoryDefinitionInstance(classLoader, repository)
        } catch (e: Exception) {
            throw MojoExecutionException("Failed to load API definition class", e)
        }

        val targetPath = Path(project.build.directory + "/generated-sources/koil")

        val tables = repositoryDefinition.tables()
        log.info("Generating ${tables.size} repositories")

        for (table in tables) {
            generateDbModel(repository.basePackage, table).writeTo(targetPath)
            generateRepository(repository.basePackage, table).writeTo(targetPath)
        }
    }

    private fun loadRepositoryDefinitionInstance(
        classLoader: ClassLoader,
        repository: RepositoryConfig
    ): RepositoryDefinition {
        val cls = classLoader.loadClass(repository.definitionClass)
        val ctor = cls.getConstructor()
        return ctor.newInstance() as RepositoryDefinition
    }
}
