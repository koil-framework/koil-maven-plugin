package io.koil.maven.server

import com.fasterxml.jackson.databind.JsonNode
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.koil.core.Failable
import io.koil.maven.common.GeneratorContext
import io.koil.web.Problem
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.EndpointContentType
import io.koil.web.webdef.EndpointHandlerReturnType
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.RoutingContext

private fun failableType(type: TypeName): TypeName =
    Failable::class.asTypeName().parameterizedBy(type, Problem::class.asTypeName())

private fun FunSpec.Builder.returnsFailable(type: TypeName, description: String): FunSpec.Builder = apply {
    returns(failableType(type), CodeBlock.of(description))
}

fun generateController(ctx: GeneratorContext, endpoints: List<Endpoint>): FileSpec {
    val controllerType = TypeSpec.interfaceBuilder(ctx.controllerClassName(endpoints[0].controllerName))

    for (endpoint in endpoints) {
        val endpointFun = FunSpec.builder(endpoint.methodName)
            .addModifiers(KModifier.ABSTRACT, KModifier.SUSPEND)
            .addKdoc(endpoint.description)

        // Make sure the following block adds the information in the same order as the block in RouterGeneration
        // Block start

        // Always add the routing context
        endpointFun.addParameter("routingContext", RoutingContext::class)

        if (endpoint.requestBody.type != EndpointContentType.NONE && !endpoint.requestBody.stream) {
            endpointFun.addParameter(
                "requestBody",
                when (endpoint.requestBody.type) {
                    EndpointContentType.TEXT -> String::class.asTypeName()
                    EndpointContentType.BINARY -> Buffer::class.asTypeName()
                    EndpointContentType.JSON -> {
                        if (endpoint.requestBody.schema != null) {
                            ctx.resolveType(endpoint.requestBody.schema!!)
                        } else {
                            JsonNode::class.asTypeName()
                        }
                    }

                    EndpointContentType.NONE -> throw IllegalStateException("This can't happen")
                }
            )
        }

        for (param in endpoint.params) {
            val type = ctx.resolveType(param.schema)
            val resolved = if (!param.required && param.defaultValue == null) type.copy(true)
            else type
            endpointFun.addParameter(
                ParameterSpec.builder(param.name, resolved)
                    .addKdoc(param.description)
                    .build()
            )
        }
        // Block end

        val mainResponseType = endpoint.mainResponse.schema?.let { ctx.resolveType(it) }
            ?: Unit::class.asTypeName()

        when (endpoint.returnType) {
            EndpointHandlerReturnType.UNIT ->
                endpointFun.returns(Unit::class, CodeBlock.of(endpoint.mainResponse.description))

            EndpointHandlerReturnType.PROBLEM ->
                endpointFun.returns(
                    Problem::class.asTypeName().copy(nullable = true),
                    CodeBlock.of(endpoint.mainResponse.description)
                )

            EndpointHandlerReturnType.FAILABLE ->
                endpointFun.returnsFailable(mainResponseType, endpoint.mainResponse.description)

            EndpointHandlerReturnType.RESPONSE_TYPE ->
                endpointFun.returns(mainResponseType, CodeBlock.of(endpoint.mainResponse.description))
        }

        controllerType.addFunction(endpointFun.build())
    }

    return FileSpec.builder(ctx.controllerClassName(endpoints[0].controllerName))
        .addType(controllerType.build())
        .build()
}
