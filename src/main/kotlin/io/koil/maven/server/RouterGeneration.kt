package io.koil.maven.server

import com.squareup.kotlinpoet.*
import io.koil.di.DiContainer
import io.koil.maven.common.GeneratorContext
import io.koil.schemas.StringJsonSchema
import io.koil.web.server.*
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.EndpointContentType
import io.koil.web.webdef.MiddlewareFactory
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router

private fun controllerNameToVar(name: String): String = name.first().lowercase() + name.substring(1)

fun generateRouter(
    ctx: GeneratorContext,
    apiDefinitionClass: ClassName,
    endpoints: List<Endpoint>
): FileSpec {
    val code = CodeBlock.builder()

    code.addStatement("val authorizationProcessor = diContainer.get<%T>()", AuthorizationProcessor::class)
        .addStatement("val endpoints = %T().endpoints()", apiDefinitionClass)
        .addStatement("val requestBodyValidators = diContainer.getAll<%T<Any>>()", RequestBodyValidator::class)
        .addStatement("val globalMiddlewares = diContainer.getAll<%T>(setOf(\"global\"))", MiddlewareFactory::class)
        .addStatement(
            "val errorHandler = diContainer.getNullable<%T>() ?: %M()",
            EndpointErrorHandler::class,
            MemberName("io.koil.web.server", "defaultErrorHandler")
        )
        .add("\n")

    val controllers = endpoints.map(Endpoint::controllerName).toSet()
    for (controller in controllers) {
        code.addStatement(
            "val %L = diContainer.get<%T>()",
            controllerNameToVar(controller),
            ctx.controllerClassName(controller),
        )
    }

    for ((index, endpoint) in endpoints.withIndex()) {
        val path = if (endpoint.path.startsWith("/")) endpoint.path
        else "/${endpoint.path}"

        code.add("\n")
            .add(
                "router.%L(%T.%L, %S)\n",
                if (endpoint.regexPath) "routeWithRegex" else "route",
                HttpMethod::class,
                endpoint.method.name(),
                "/${endpoint.namespace}/v${endpoint.apiVersion}$path"
            ).indent()

        if (endpoint.requiresAuthentication) {
            code.add(".handler(%T)\n", AuthenticationRequiredHandler::class)
        }
        if (endpoint.params.isNotEmpty()) code.add(
            ".handler(%T(endpoints[%L].params))\n",
            ParametersHandler::class,
            index
        )

        if (endpoint.requestBody.type != EndpointContentType.NONE && !endpoint.requestBody.stream) {
            addRequestBodyHandler(ctx, index, endpoint, code)
        }

        if (endpoint.authorizationRequirement != null) {
            code.add(
                ".handler(authorizationProcessor.getMiddleware(endpoints[%L].authorizationRequirement!!))\n",
                index
            )
        }

        code.add(
            ".%M(globalMiddlewares, endpoints[%L])\n",
            MemberName("io.koil.web.server", "registerGlobalMiddlewares"),
            index
        )

        if (endpoint.middlewares.isNotEmpty()) {
            code.add(
                ".%M(diContainer, endpoints[%L])\n",
                MemberName("io.koil.web.server", "registerEndpointMiddlewares"),
                index
            )
        }

        code.add(
            ".%M(endpoints[%L].noResponseProcessing, errorHandler) { ctx ->\n",
            MemberName("io.koil.web.server", "registerCoroutineControllerHandler"),
            index,
        )
            .indent()
            .add("%L.%L(\n", controllerNameToVar(endpoint.controllerName), endpoint.methodName)
            .indent()

        // Make sure this block adds the information in the same order as the block in ControllerGeneration
        // Block start

        // Always add the routing context
        code.add("ctx,\n")

        if (endpoint.requestBody.type != EndpointContentType.NONE && !endpoint.requestBody.stream) {
            code.add("ctx[\"koil_reqBody\"],\n")
        }

        for (param in endpoint.params) {
            val notNull = param.required || param.defaultValue != null

            // Transform the parameter on-site if the parameter is a string enum
            // And yes, it's the simplest way to do that :(

            val schema = param.schema
            val enumTypeName = if (schema is StringJsonSchema && schema.enumSourceName != null) {
                ctx.resolveType(schema)
            } else null

            if (notNull && enumTypeName != null) {
                code.add("%T.valueOf(", enumTypeName)
            }

            // Add the call to the getParameter function
            code.add("ctx.%M", MemberName("io.koil.web.webdef", "getParameter"))

            // We have to specify the String generic type when we want to convert to enum
            if (enumTypeName != null) code.add("<%T>", String::class)

            // Add the parameter name argument
            code.add("(%S)", param.name)

            if (notNull) {
                code.add("!!")
                if (enumTypeName != null) {
                    code.add(".uppercase())")
                }
            } else {
                if (enumTypeName != null) {
                    code.add("?.let { strParam -> %T.valueOf(strParam.uppercase()) }", enumTypeName)
                }
            }

            code.add(",\n")
        }
        // Block end

        code.unindent()
            .add(")\n")
            .unindent()
            .add("}\n")
            .unindent()
    }

    return FileSpec.builder(ctx.routerFactoryClassName(apiDefinitionClass.simpleName))
        .addType(
            TypeSpec.classBuilder(ctx.routerFactoryClassName(apiDefinitionClass.simpleName))
                .addSuperinterface(RouterFactory::class)
                .addFunction(
                    FunSpec.builder("register")
                        .addModifiers(KModifier.SUSPEND, KModifier.OVERRIDE)
                        .addParameter(ParameterSpec("diContainer", DiContainer::class.asTypeName()))
                        .addParameter(ParameterSpec("router", Router::class.asTypeName()))
                        .addCode(code.build())
                        .build()
                )
                .build()
        )
        .build()
}

private fun addRequestBodyHandler(ctx: GeneratorContext, index: Int, endpoint: Endpoint, code: CodeBlock.Builder) {
    code.add(".handler(\n")
        .indent()
        .add("%T(\n", RequestBodyHandler::class)
        .indent()
        .add("endpoints[%L].requestBody.type,\n", index)
        .add("endpoints[%L].requestBody.expectedContentType,\n", index)

    if (endpoint.requestBody.schema == null) {
        code.add("null,\n")
            .add("null,\n")
            .add("null,\n")
    } else {
        val requestBodyType = ctx.resolveType(endpoint.requestBody.schema!!)
        code.add("endpoints[%L].requestBody.schema!!,\n", index)
            .add("%T::class.java,\n", requestBodyType)
            .add("findRequestBodyValidator(requestBodyValidators, %T::class.java),\n", requestBodyType)
    }

    code.unindent()
        .add(")\n")
        .unindent()
        .add(")\n")
}
