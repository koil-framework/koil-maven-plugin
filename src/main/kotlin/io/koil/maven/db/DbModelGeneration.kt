package io.koil.maven.db

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.json.JsonMapper
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.koil.db.*
import io.vertx.core.buffer.Buffer
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*

private fun typeFromColumnType(type: TableColumnType, enumClass: Class<*>?): TypeName = when (type) {
    TableColumnType.INT -> Int::class.asTypeName()
    TableColumnType.FLOAT -> Float::class.asTypeName()
    TableColumnType.LONG -> Long::class.asTypeName()
    TableColumnType.STRING -> String::class.asTypeName()
    TableColumnType.BOOLEAN -> Boolean::class.asTypeName()
    TableColumnType.DOUBLE -> Double::class.asTypeName()
    TableColumnType.UUID -> UUID::class.asTypeName()
    TableColumnType.INSTANT -> Instant::class.asTypeName()
    TableColumnType.OFFSET_DATETIME -> OffsetDateTime::class.asTypeName()
    TableColumnType.BINARY -> Buffer::class.asTypeName()
    TableColumnType.ENUM -> enumClass!!.asTypeName()
    TableColumnType.JSON -> JsonNode::class.asTypeName()
    TableColumnType.ARRAY -> throw RuntimeException("Nested arrays are not supported")
}

fun typeFromColumnType(column: TableColumn): TypeName {
    val cls =
        if (column.propertyType != TableColumnType.ARRAY) typeFromColumnType(column.propertyType, column.enumClass)
        else Array::class.asTypeName()
            .parameterizedBy(typeFromColumnType(column.propertyArrayItemType!!, column.enumClass))

    return if (column.nullable) cls.copy(nullable = true) else cls
}

fun addTupleValue(codeBlock: CodeBlock.Builder, column: TableColumn, lineEnding: Boolean = true) {
    when (column.propertyType) {
        TableColumnType.INT,
        TableColumnType.FLOAT,
        TableColumnType.STRING,
        TableColumnType.LONG,
        TableColumnType.DOUBLE,
        TableColumnType.BOOLEAN,
        TableColumnType.OFFSET_DATETIME,
        TableColumnType.BINARY,
        TableColumnType.ARRAY,
        TableColumnType.UUID -> codeBlock.add("%L", column.propertyName)

        TableColumnType.ENUM -> codeBlock.add("%L%L.name", column.propertyName, if (column.nullable) "?" else "")

        TableColumnType.INSTANT -> codeBlock.add(
            "%L%L.atOffset(%T.UTC)",
            column.propertyName,
            if (column.nullable) "?" else "",
            ZoneOffset::class
        )

        TableColumnType.JSON -> codeBlock.add(
            "%L%L.toString()",
            column.propertyName,
            if (column.nullable) "?" else "",
        )
    }

    if (lineEnding) codeBlock.add(",\n")
}

private fun arrayFromRow(codeBlock: CodeBlock.Builder, column: TableColumn): CodeBlock.Builder =
    when (column.propertyArrayItemType!!) {
        TableColumnType.INT -> codeBlock.add("row.getArrayOfIntegers(prefix + %S),\n", column.name)
        TableColumnType.FLOAT -> codeBlock.add("row.getArrayOfFloats(prefix + %S),\n", column.name)
        TableColumnType.STRING -> codeBlock.add("row.getArrayOfStrings(prefix + %S),\n", column.name)
        TableColumnType.LONG -> codeBlock.add("row.getArrayOfLongs(prefix + %S),\n", column.name)
        TableColumnType.DOUBLE -> codeBlock.add("row.getArrayOfDoubles(prefix + %S),\n", column.name)
        TableColumnType.BOOLEAN -> codeBlock.add("row.getArrayOfBooleans(prefix + %S),\n", column.name)
        TableColumnType.OFFSET_DATETIME -> codeBlock.add("row.getArrayOfOffsetDateTimes(prefix + %S),\n", column.name)

        TableColumnType.UUID -> codeBlock.add("row.getArrayOfUUIDs(prefix + %S),\n", column.name)
        TableColumnType.BINARY -> codeBlock.add("row.getArrayOfBuffers(prefix + %S),\n", column.name)
        TableColumnType.ARRAY -> throw RuntimeException("Nested arrays are not supported")

        TableColumnType.ENUM -> TODO()
        TableColumnType.INSTANT -> TODO()
        TableColumnType.JSON -> TODO()
    }

private fun fromRow(codeBlock: CodeBlock.Builder, column: TableColumn): CodeBlock.Builder = when (column.propertyType) {
    TableColumnType.INT -> codeBlock.add("row.getInteger(prefix + %S),\n", column.name)
    TableColumnType.FLOAT -> codeBlock.add("row.getFloat(prefix + %S),\n", column.name)
    TableColumnType.STRING -> codeBlock.add("row.getString(prefix + %S),\n", column.name)
    TableColumnType.LONG -> codeBlock.add("row.getLong(prefix + %S),\n", column.name)
    TableColumnType.DOUBLE -> codeBlock.add("row.getDouble(prefix + %S),\n", column.name)
    TableColumnType.BOOLEAN -> codeBlock.add("row.getBoolean(prefix + %S),\n", column.name)
    TableColumnType.OFFSET_DATETIME -> codeBlock.add("row.getOffsetDateTime(prefix + %S),\n", column.name)
    TableColumnType.UUID -> codeBlock.add("row.getUUID(prefix + %S),\n", column.name)
    TableColumnType.BINARY -> codeBlock.add("row.getBuffer(prefix + %S),\n", column.name)
    TableColumnType.JSON -> codeBlock.add("jsonMapper.readTree(row.getString(prefix + %S)),\n", column.name)
    TableColumnType.ARRAY -> arrayFromRow(codeBlock, column)

    TableColumnType.ENUM ->
        codeBlock.add(
            "row.getString(prefix + %S)%L.let(%T::valueOf),\n",
            column.name,
            if (column.nullable) "?" else "",
            column.enumClass!!,
        )

    TableColumnType.INSTANT -> codeBlock.add(
        "row.getLocalDateTime(prefix + %S)%L.toInstant(%T.UTC),\n",
        column.name,
        if (column.nullable) "?" else "",
        ZoneOffset::class
    )
}

val tupleClass = ClassName("io.vertx.sqlclient", "Tuple")
val rowClass = ClassName("io.vertx.sqlclient", "Row")

private fun createTupleMethod(name: String, code: CodeBlock): FunSpec = FunSpec.builder(name)
    .returns(tupleClass)
    .addCode(
        CodeBlock.builder()
            .add("return %T.of(\n", tupleClass)
            .indent()
            .add(code)
            .unindent()
            .add(")\n")
            .build()
    )
    .build()

private fun parameterFromColumn(column: TableColumn): ParameterSpec {
    val param = ParameterSpec.builder(column.propertyName, typeFromColumnType(column))
    if (column.description != null) param.addKdoc(column.description!!)
    return param.build()
}

private fun propertyFromColumn(column: TableColumn): PropertySpec =
    PropertySpec.builder(column.propertyName, typeFromColumnType(column))
        .initializer(column.propertyName)
        .build()

fun generateDbModel(basePackage: String, schema: TableSchema): FileSpec {
    val className = ClassName("$basePackage.model", schema.objectName)

    val ctorSpec = FunSpec.constructorBuilder()
    val properties = mutableListOf<PropertySpec>()
    val creationTupleCode = CodeBlock.builder()
    val updateTupleCode = CodeBlock.builder()
    val fromRowCode = CodeBlock.builder()
    val selectColumnListCode = CodeBlock.builder()
        .addStatement("val tableName = tableAlias ?: %S", schema.name)
        .add("return arrayOf(\n")
        .indent()

    var hasJson = false
    var hasEditableProperties = false
    for (column in schema.columns) {
        ctorSpec.addParameter(parameterFromColumn(column))
        properties.add(propertyFromColumn(column))

        addTupleValue(creationTupleCode, column)
        if (column.editable || column.role == TableColumnRole.PRIMARY_KEY) {
            if (column.editable) hasEditableProperties = true
            addTupleValue(updateTupleCode, column)
        }

        if (column.propertyType == TableColumnType.JSON) hasJson = true
        fromRow(fromRowCode, column)
        selectColumnListCode.add("tableName + %S + prefix + %S,\n", ".${column.name} AS ", column.name)
    }

    selectColumnListCode.unindent().add(")")

    val fromRowFn = FunSpec.builder("fromRow")
    if (hasJson) fromRowFn.addParameter(ParameterSpec("jsonMapper", JsonMapper::class.asTypeName()))
    fromRowFn.addParameter(ParameterSpec("row", rowClass))
        .addParameter(
            ParameterSpec.builder("prefix", String::class)
                .defaultValue("\"\"")
                .build()
        )
        .returns(className)
        .addCode(
            CodeBlock.builder()
                .add("return %T(\n", className)
                .indent()
                .add(fromRowCode.build())
                .unindent()
                .add(")\n")
                .build()
        )

    val selectColumnListFn = FunSpec.builder("selectColumnList")
        .addParameter(
            ParameterSpec.builder("prefix", String::class)
                .defaultValue("\"\"")
                .build()
        ).addParameter(
            ParameterSpec.builder("tableAlias", String::class.asTypeName().copy(nullable = true))
                .defaultValue("null")
                .build()
        ).returns(Array::class.asTypeName().parameterizedBy(String::class.asTypeName()))
        .addCode(selectColumnListCode.build())
        .build()

    val typeSpec = TypeSpec.classBuilder(className)
        .addModifiers(KModifier.DATA)
        .primaryConstructor(ctorSpec.build())
        .addProperties(properties)
        .addKdoc(schema.description ?: "")
        .addType(
            TypeSpec.companionObjectBuilder()
                .addFunction(fromRowFn.build())
                .addFunction(selectColumnListFn)
                .build()
        )

    typeSpec.addFunction(createTupleMethod("toCreationTuple", creationTupleCode.build()))
    if (hasEditableProperties) {
        typeSpec.addFunction(createTupleMethod("toUpdateTuple", updateTupleCode.build()))
    }

    val fileSpec = FileSpec.builder(className)

    val primaryKeyColumns = schema.columns.filter { it.role == TableColumnRole.PRIMARY_KEY }
    if (primaryKeyColumns.size > 1) {
        val keyClass = ClassName("$basePackage.model", schema.objectName + "Key")
        val toKeyCode = CodeBlock.builder()
        val keyTupleCode = CodeBlock.builder()
        val keyCtorSpec = FunSpec.constructorBuilder()
        val keyProperties = mutableListOf<PropertySpec>()

        for (idColumn in primaryKeyColumns) {
            keyCtorSpec.addParameter(parameterFromColumn(idColumn))
            keyProperties.add(propertyFromColumn(idColumn))
            addTupleValue(keyTupleCode, idColumn)
            toKeyCode.add("%L,\n", idColumn.propertyName)
        }

        typeSpec.addFunction(
            FunSpec.builder("toKey")
                .returns(keyClass)
                .addCode(
                    CodeBlock.builder()
                        .add("return %T(\n", keyClass)
                        .indent()
                        .add(toKeyCode.build())
                        .unindent()
                        .add(")")
                        .build()
                ).build()
        )
        fileSpec.addType(
            TypeSpec.classBuilder(keyClass)
                .addModifiers(KModifier.DATA)
                .addKdoc("The key to identify a unique ${schema.objectName}")
                .primaryConstructor(keyCtorSpec.build())
                .addProperties(keyProperties)
                .addFunction(createTupleMethod("toTuple", keyTupleCode.build()))
                .build()
        )
    }

    return fileSpec
        .addType(typeSpec.build())
        .build()
}
