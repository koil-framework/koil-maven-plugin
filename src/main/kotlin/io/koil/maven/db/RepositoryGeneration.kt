package io.koil.maven.db

import com.fasterxml.jackson.databind.json.JsonMapper
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.koil.db.*
import io.vertx.core.Future

// TODO An overhaul is needed to make some parts more generic

// TODO Add search methods
// TODO Add soft delete filter with param on find* methods

private val sqlClientClass = ClassName("io.vertx.sqlclient", "SqlClient")
private val rowSetClass = ClassName("io.vertx.sqlclient", "RowSet").parameterizedBy(rowClass)

fun generateRepository(basePackage: String, schema: TableSchema): FileSpec {
    val interfaceName = ClassName("$basePackage.repositories", schema.objectName + "CrudRepository")
    val implClassName = ClassName("$basePackage.repositories", "Postgres${schema.objectName}CrudRepository")
    val modelClassName = ClassName("$basePackage.model", schema.objectName)

    val primaryKeyColumns = schema.columns.filter { it.role == TableColumnRole.PRIMARY_KEY }
    // We need primary key columns in update columns even though they're not technically used
    // because we need to make sure the positional arguments index are correct
    val updateColumns = schema.columns.filter {
        it.editable || it.role == TableColumnRole.LAST_MODIFICATION_TIMESTAMP || it.role == TableColumnRole.PRIMARY_KEY
    }
    val editable = schema.columns.firstOrNull { it.editable } != null
    val hasSoftDelete = schema.columns.firstOrNull { it.role === TableColumnRole.DELETION_FLAG } != null
    val hasJson = schema.columns.firstOrNull { it.propertyType == TableColumnType.JSON } != null

    val interfaceSpec = TypeSpec.interfaceBuilder(interfaceName)
        .addKdoc(schema.description ?: "")
        .addFunction(findAllPrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(findAllPaginatedPrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(findByIdPrototype(modelClassName, primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(findAllByIdsPrototype(modelClassName, primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(createPrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(createAllPrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(hardDeletePrototype(primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build())
        .addFunction(hardDeleteAllPrototype(modelClassName, primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build())
    if (hasSoftDelete) {
        interfaceSpec.addFunction(softDeletePrototype(primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build())
            .addFunction(
                softDeleteAllPrototype(modelClassName, primaryKeyColumns).addModifiers(KModifier.ABSTRACT).build()
            )
    }
    if (editable) {
        interfaceSpec.addFunction(updatePrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
            .addFunction(updateAllPrototype(modelClassName).addModifiers(KModifier.ABSTRACT).build())
    }

    val implClassConstructor = FunSpec.constructorBuilder()
        .addParameter(ParameterSpec("sqlClient", sqlClientClass))
    if (hasJson) implClassConstructor.addParameter(ParameterSpec("jsonMapper", JsonMapper::class.asTypeName()))

    val implClassSpec = TypeSpec.classBuilder(implClassName)
        .addSuperinterface(interfaceName)
        .addModifiers(KModifier.OPEN)
        .primaryConstructor(implClassConstructor.build())
        .addProperty(
            PropertySpec.builder("sqlClient", sqlClientClass, KModifier.PROTECTED)
                .initializer("sqlClient")
                .build()
        )
        .addFunction(mapAllRowsSpec(modelClassName, hasJson))
        .addFunction(mapFirstRowSpec(modelClassName, hasJson))
        .addFunction(collectAllSpec(modelClassName, hasJson))
        .addFunction(collectFirstSpec(modelClassName, hasJson))
        .addFunction(findAllSpec(modelClassName, schema))
        .addFunction(findAllPaginatedSpec(modelClassName, schema))
        .addFunction(findByIdSpec(modelClassName, primaryKeyColumns, schema))
        .addFunction(findAllByIdsSpec(modelClassName, primaryKeyColumns, schema))
        .addFunction(createSpec(modelClassName, schema))
        .addFunction(createAllSpec(modelClassName, schema))
        .addFunction(hardDeleteSpec(primaryKeyColumns, schema))
        .addFunction(hardDeleteAllSpec(modelClassName, primaryKeyColumns, schema))
    if (hasSoftDelete) {
        implClassSpec.addFunction(softDeleteSpec(primaryKeyColumns, schema))
            .addFunction(softDeleteAllSpec(modelClassName, primaryKeyColumns, schema))
    }
    if (editable) {
        implClassSpec.addFunction(updateSpec(modelClassName, primaryKeyColumns, updateColumns, schema))
            .addFunction(updateAllSpec(modelClassName, primaryKeyColumns, updateColumns, schema))
    }

    if (hasJson) {
        implClassSpec.addProperty(
            PropertySpec.builder("jsonMapper", JsonMapper::class, KModifier.PROTECTED)
                .initializer("jsonMapper")
                .build()
        )
    }

    return FileSpec.builder(interfaceName)
        .addType(interfaceSpec.build())
        .addType(implClassSpec.build())
        .build()
}

// Repository helper methods

private fun mapAllRowsSpec(modelClassName: ClassName, hasJson: Boolean): FunSpec = FunSpec.builder("mapAllRows")
    .addModifiers(KModifier.PROTECTED)
    .addKdoc("Returns all results from the first row set")
    .addParameter(ParameterSpec("rowSet", rowSetClass))
    .returns(List::class.asTypeName().parameterizedBy(modelClassName))
    .addStatement("return rowSet.map { %T.fromRow(%Lit) }", modelClassName, if (hasJson) "jsonMapper, " else "")
    .build()

private fun mapFirstRowSpec(modelClassName: ClassName, hasJson: Boolean): FunSpec = FunSpec.builder("mapFirstRow")
    .addModifiers(KModifier.PROTECTED)
    .addKdoc("Returns the first result from the first row set")
    .addParameter(ParameterSpec("rowSet", rowSetClass))
    .returns(modelClassName.copy(nullable = true))
    .addStatement("return rowSet.first()?.let { %T.fromRow(%Lit) }", modelClassName, if (hasJson) "jsonMapper, " else "")
    .build()

private fun collectAllSpec(modelClassName: ClassName, hasJson: Boolean): FunSpec = FunSpec.builder("collectAll")
    .addModifiers(KModifier.PROTECTED)
    .addKdoc("Returns all results from all row sets")
    .addParameter(ParameterSpec("rowSet", rowSetClass))
    .returns(List::class.asTypeName().parameterizedBy(modelClassName))
    .addStatement("val results = mutableListOf<%T>()", modelClassName)
    .addStatement("var currentSet: %T? = rowSet", rowSetClass)
    .beginControlFlow("while (currentSet != null)")
    .addStatement("results.addAll(currentSet.map { %T.fromRow(%Lit) })", modelClassName, if (hasJson) "jsonMapper, " else "")
    .addStatement("currentSet = currentSet.next()")
    .endControlFlow()
    .addStatement("return results", modelClassName)
    .build()

private fun collectFirstSpec(modelClassName: ClassName, hasJson: Boolean): FunSpec = FunSpec.builder("collectFirst")
    .addModifiers(KModifier.PROTECTED)
    .addKdoc("Returns the first result from each row set")
    .addParameter(ParameterSpec("rowSet", rowSetClass))
    .returns(List::class.asTypeName().parameterizedBy(modelClassName))
    .addStatement("val results = mutableListOf<%T>()", modelClassName)
    .addStatement("var currentSet: %T? = rowSet", rowSetClass)
    .beginControlFlow("while (currentSet != null)")
    .addStatement("currentSet.firstOrNull()?.let { results.add(%T.fromRow(%Lit)) }", modelClassName, if (hasJson) "jsonMapper, " else "")
    .addStatement("currentSet = currentSet.next()")
    .endControlFlow()
    .addStatement("return results", modelClassName)
    .build()

// Generic SQL request builder helpers

private fun keyClassName(modelClassName: ClassName): ClassName =
    ClassName(modelClassName.packageName, modelClassName.simpleName + "Key")

private fun buildCondition(query: ConditionalSqlQuery, columns: List<TableColumn>, arrayInput: Boolean = false) {
    for ((index, idColumn) in columns.withIndex()) {
        var condition = "${idColumn.name} = "

        if (arrayInput) condition += "ANY("
        condition += "\$${index + 1}"
        if (arrayInput) condition += ")"

        query.where(condition)
    }
}

private fun generateFindAllSingleColumn(schema: TableSchema, column: TableColumn, paramName: String): CodeBlock {
    return CodeBlock.builder()
        .addStatement("if (%L.isEmpty()) return %T.succeededFuture(emptyList())", paramName, Future::class)
        .add(
            "return sqlClient.preparedQuery(%P)\n",
            "SELECT * FROM ${schema.tableIdentifier} WHERE ${column.name} = ANY($1)",
        )
        .add(".execute(%T.of(%L.toTypedArray()))\n", tupleClass, paramName)
        .add(".map(this::mapAllRows)\n")
        .build()
}

private fun generateFindAllMultipleColumns(
    schema: TableSchema,
    columns: List<TableColumn>,
    paramName: String,
    paramType: ClassName,
): CodeBlock {
    val query = SelectSqlQuery()
        .select("*")
        .from(schema.tableIdentifier)
    buildCondition(query, columns)

    return CodeBlock.builder()
        .addStatement("if (%L.isEmpty()) return %T.succeededFuture(emptyList())", paramName, Future::class)
        .add("return sqlClient.preparedQuery(%P)\n", query.build())
        .add(".executeBatch(%L.map(%T::toTuple))\n", paramName, paramType)
        .add(".map(this::collectFirst)\n")
        .build()
}

private fun buildUpdate(query: UpdateSqlQuery, columns: List<TableColumn>) {
    var index = 1
    for (column in columns) {
        if (column.role == TableColumnRole.PRIMARY_KEY) {
            index++
            continue
        }

        if (column.role == TableColumnRole.LAST_MODIFICATION_TIMESTAMP) {
            query.set(column.name, "NOW()")
        } else {
            query.set(column.name, index++)
        }
    }
}

private fun buildPrimaryKeyTupleCode(primaryKeyColumns: List<TableColumn>): CodeBlock {
    val tupleCode = CodeBlock.builder()
    for ((index, column) in primaryKeyColumns.withIndex()) {
        if (index != 0) tupleCode.add(", ")
        addTupleValue(tupleCode, column, false)
    }
    return tupleCode.build()
}

private fun primaryKeyParams(primaryKeyColumns: List<TableColumn>): List<ParameterSpec> = primaryKeyColumns.map {
    ParameterSpec(it.propertyName, typeFromColumnType(it))
}

// Actual repository construction

private fun findAllPrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("findAll")
    .returns(Future::class.asTypeName().parameterizedBy(List::class.asTypeName().parameterizedBy(modelClassName)))

private fun findAllSpec(modelClassName: ClassName, schema: TableSchema): FunSpec {
    return findAllPrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.query(%S)\n", "SELECT * FROM " + schema.tableIdentifier)
                .add(".execute()\n")
                .add(".map(this::mapAllRows)\n")
                .build()
        )
        .build()
}

private fun findAllPaginatedPrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("findAllPaginated")
    .addParameter(
        ParameterSpec.builder("page", Int::class.asTypeName()).addKdoc("The page to get, starting at 1").build()
    )
    .addParameter(
        ParameterSpec.builder("pageSize", Int::class.asTypeName()).addKdoc("How many items to get per page").build()
    )
    .returns(Future::class.asTypeName().parameterizedBy(List::class.asTypeName().parameterizedBy(modelClassName)))

private fun findAllPaginatedSpec(modelClassName: ClassName, schema: TableSchema): FunSpec {
    return findAllPaginatedPrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .addStatement("val offset = (page - 1) * pageSize")
                .add(
                    "return sqlClient.query(%P)\n",
                    "SELECT * FROM ${schema.tableIdentifier} OFFSET \$offset LIMIT \$pageSize",
                )
                .add(".execute()\n")
                .add(".map(this::mapAllRows)\n")
                .build()
        )
        .build()
}

private fun findByIdPrototype(modelClassName: ClassName, primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    FunSpec.builder("findById")
        .addParameters(primaryKeyParams(primaryKeyColumns))
        .returns(Future::class.asTypeName().parameterizedBy(modelClassName.copy(nullable = true)))

private fun findByIdSpec(
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>,
    schema: TableSchema
): FunSpec {
    val query = SelectSqlQuery()
        .select("*")
        .from(schema.tableIdentifier)
    buildCondition(query, primaryKeyColumns)

    return findByIdPrototype(modelClassName, primaryKeyColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", query.build())
                .add(".execute(%T.of(", tupleClass)
                .add(buildPrimaryKeyTupleCode(primaryKeyColumns))
                .add("))\n")
                .add(".map(this::mapFirstRow)\n")
                .build()
        )
        .build()
}

private fun findAllByIdsPrototype(modelClassName: ClassName, idColumns: List<TableColumn>): FunSpec.Builder {
    val subType = if (idColumns.size > 1) keyClassName(modelClassName)
    else typeFromColumnType(idColumns[0])

    return FunSpec.builder("findAllByIds")
        .addParameter(ParameterSpec("keys", Set::class.asTypeName().parameterizedBy(subType)))
        .returns(Future::class.asTypeName().parameterizedBy(List::class.asTypeName().parameterizedBy(modelClassName)))
}

private fun findAllByIdsSpec(modelClassName: ClassName, idColumns: List<TableColumn>, schema: TableSchema): FunSpec {
    val code = if (idColumns.size > 1) generateFindAllMultipleColumns(
        schema,
        idColumns,
        "keys",
        keyClassName(modelClassName)
    ) else generateFindAllSingleColumn(schema, idColumns[0], "keys")

    return findAllByIdsPrototype(modelClassName, idColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(code)
        .build()
}

private fun buildCreateQuery(schema: TableSchema): String {
    return InsertSqlQuery(schema.tableIdentifier)
        .columns(schema.columns.map(TableColumn::name))
        .values(schema.columns.indices.map { "\$${it + 1}" })
        .build()
}

private fun createPrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("create")
    .addParameter(ParameterSpec("obj", modelClassName))
    .returns(Future::class.parameterizedBy(Unit::class))

private fun createSpec(modelClassName: ClassName, schema: TableSchema): FunSpec {
    return createPrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", buildCreateQuery(schema))
                .add(".execute(obj.toCreationTuple())\n")
                .add(".map(Unit)")
                .build()
        )
        .build()
}

private fun createAllPrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("createAll")
    .addParameter(ParameterSpec("objs", List::class.asTypeName().parameterizedBy(modelClassName)))
    .returns(Future::class.parameterizedBy(Unit::class))

private fun createAllSpec(modelClassName: ClassName, schema: TableSchema): FunSpec {
    return createAllPrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addStatement("if (objs.isEmpty()) return %T.succeededFuture(Unit)", Future::class)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", buildCreateQuery(schema))
                .add(".executeBatch(objs.map(%T::toCreationTuple))\n", modelClassName)
                .add(".map(Unit)")
                .build()
        )
        .build()
}

private fun updatePrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("update")
    .addParameter(ParameterSpec("obj", modelClassName))
    .returns(Future::class.parameterizedBy(Boolean::class))

private fun updateSpec(
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>,
    updateColumns: List<TableColumn>,
    schema: TableSchema
): FunSpec {
    val query = UpdateSqlQuery(schema.tableIdentifier)
    buildUpdate(query, updateColumns)
    buildCondition(query, primaryKeyColumns)

    return updatePrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", query.build())
                .add(".execute(obj.toUpdateTuple())\n")
                .add(".map { it.rowCount() == 1 }")
                .build()
        )
        .build()
}

private fun updateAllPrototype(modelClassName: ClassName): FunSpec.Builder = FunSpec.builder("updateAll")
    .addParameter(ParameterSpec("objs", List::class.asTypeName().parameterizedBy(modelClassName)))
    .returns(Future::class.parameterizedBy(Unit::class))

private fun updateAllSpec(
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>,
    updateColumns: List<TableColumn>,
    schema: TableSchema
): FunSpec {
    val query = UpdateSqlQuery(schema.tableIdentifier)
    buildUpdate(query, updateColumns)
    buildCondition(query, primaryKeyColumns)

    return updateAllPrototype(modelClassName)
        .addModifiers(KModifier.OVERRIDE)
        .addStatement("if (objs.isEmpty()) return %T.succeededFuture(Unit)", Future::class)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", query.build())
                .add(".executeBatch(objs.map(%T::toUpdateTuple))\n", modelClassName)
                .add(".map(Unit)")
                .build()
        )
        .build()
}

private fun buildHardDeleteQuery(
    primaryKeyColumns: List<TableColumn>,
    schema: TableSchema,
    batch: Boolean = false,
): String {
    val query = DeleteSqlQuery(schema.tableIdentifier)
    buildCondition(query, primaryKeyColumns, primaryKeyColumns.size == 1 && batch)
    return query.build()
}

private fun buildSoftDeleteQuery(
    primaryKeyColumns: List<TableColumn>,
    schema: TableSchema,
    batch: Boolean = false,
): String {
    val query = UpdateSqlQuery(schema.tableIdentifier)

    val deletionFlagColumn = schema.columns.firstOrNull { it.role == TableColumnRole.DELETION_FLAG }
        ?: throw RuntimeException("Soft deletion is enabled but no column with the DELETION_FLAG role is configured")
    query.set(deletionFlagColumn.name, "NOT ${deletionFlagColumn.name}")

    buildCondition(query, primaryKeyColumns, primaryKeyColumns.size == 1 && batch)
    return query.build()
}

private fun deletePrototype(name: String, primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    FunSpec.builder(name)
        .addParameters(primaryKeyParams(primaryKeyColumns))
        .returns(Future::class.parameterizedBy(Boolean::class))

private fun hardDeletePrototype(primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    deletePrototype("hardDelete", primaryKeyColumns)

private fun softDeletePrototype(primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    deletePrototype("softDelete", primaryKeyColumns)

private fun hardDeleteSpec(primaryKeyColumns: List<TableColumn>, schema: TableSchema): FunSpec {
    return hardDeletePrototype(primaryKeyColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", buildHardDeleteQuery(primaryKeyColumns, schema))
                .add(".execute(%T.of(", tupleClass)
                .add(buildPrimaryKeyTupleCode(primaryKeyColumns))
                .add("))\n")
                .add(".map { it.rowCount() == 1 }")
                .build()
        )
        .build()
}

private fun softDeleteSpec(primaryKeyColumns: List<TableColumn>, schema: TableSchema): FunSpec {
    return softDeletePrototype(primaryKeyColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addCode(
            CodeBlock.builder()
                .add("return sqlClient.preparedQuery(%P)\n", buildSoftDeleteQuery(primaryKeyColumns, schema))
                .add(".execute(%T.of(", tupleClass)
                .add(buildPrimaryKeyTupleCode(primaryKeyColumns))
                .add("))\n")
                .add(".map { it.rowCount() == 1 }")
                .build()
        )
        .build()
}

private fun deleteAllPrototype(
    name: String,
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>
): FunSpec.Builder {
    val subType =
        if (primaryKeyColumns.size > 1) keyClassName(modelClassName)
        else typeFromColumnType(primaryKeyColumns[0])

    return FunSpec.builder(name)
        .addParameter(ParameterSpec("keys", Set::class.asTypeName().parameterizedBy(subType)))
        .returns(Future::class.parameterizedBy(Unit::class))
}

private fun hardDeleteAllPrototype(modelClassName: ClassName, primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    deleteAllPrototype("hardDeleteAll", modelClassName, primaryKeyColumns)

private fun softDeleteAllPrototype(modelClassName: ClassName, primaryKeyColumns: List<TableColumn>): FunSpec.Builder =
    deleteAllPrototype("softDeleteAll", modelClassName, primaryKeyColumns)

private fun hardDeleteAllSpec(
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>,
    schema: TableSchema
): FunSpec {
    val code = CodeBlock.builder()
        .add("return sqlClient.preparedQuery(%P)\n", buildHardDeleteQuery(primaryKeyColumns, schema, true))
    if (primaryKeyColumns.size > 1) code.add(".executeBatch(keys.map(%T::toTuple))\n", keyClassName(modelClassName))
    else code.add(".execute(%T.of(keys.toTypedArray()))\n", tupleClass)
    code.add(".map(Unit)")

    return hardDeleteAllPrototype(modelClassName, primaryKeyColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addStatement("if (keys.isEmpty()) return %T.succeededFuture(Unit)", Future::class)
        .addCode(code.build())
        .build()
}

private fun softDeleteAllSpec(
    modelClassName: ClassName,
    primaryKeyColumns: List<TableColumn>,
    schema: TableSchema
): FunSpec {
    val code = CodeBlock.builder()
        .add("return sqlClient.preparedQuery(%P)\n", buildSoftDeleteQuery(primaryKeyColumns, schema, true))
    if (primaryKeyColumns.size > 1) code.add(".executeBatch(keys.map(%T::toTuple))\n", keyClassName(modelClassName))
    else code.add(".execute(%T.of(keys.toTypedArray()))\n", tupleClass)
    code.add(".map(Unit)")

    return softDeleteAllPrototype(modelClassName, primaryKeyColumns)
        .addModifiers(KModifier.OVERRIDE)
        .addStatement("if (keys.isEmpty()) return %T.succeededFuture(Unit)", Future::class)
        .addCode(code.build())
        .build()
}
