package io.koil.maven.common

import io.koil.schemas.ArrayJsonSchema
import io.koil.schemas.JsonSchema
import io.koil.schemas.ObjectJsonSchema
import io.koil.schemas.StringJsonSchema

interface SchemaTypeInfo {
    /** Is this type already written to sources */
    var written: Boolean

    /** The list of source names this schema's type depends on */
    val dependsOn: Set<String>

    /** Is this type used in request bodies ? */
    val input: Boolean

    val schema: JsonSchema

    /** Returns a copy of this schema type info, setting input to true */
    fun copyAsInput(): SchemaTypeInfo

    data class Enum(
        override val schema: StringJsonSchema,
        override val dependsOn: Set<String>,
        override var written: Boolean,
        override val input: Boolean,
    ) : SchemaTypeInfo {
        override fun copyAsInput(): SchemaTypeInfo = copy(input = true)
    }

    data class Obj(
        override val schema: ObjectJsonSchema,
        override val dependsOn: Set<String>,
        override var written: Boolean,
        override val input: Boolean,
    ) : SchemaTypeInfo {
        override fun copyAsInput(): SchemaTypeInfo = copy(input = true)
    }

    data class SubType(
        override val schema: ObjectJsonSchema,
        val supertypeTypeInfo: SchemaTypeInfo,
        override val input: Boolean,
    ) : SchemaTypeInfo {
        override var written: Boolean
            get() = supertypeTypeInfo.written
            set(_) = throw IllegalStateException("Cannot set written on subtypes")

        override val dependsOn: Set<String>
            get() = supertypeTypeInfo.dependsOn

        override fun copyAsInput(): SchemaTypeInfo = copy(input = true)
    }
}

data class SchemaEntry(
    val schema: JsonSchema?,
    val typeInfo: SchemaTypeInfo?,
)

class SchemaRegistry {
    private val seen = mutableSetOf<String>()
    private val schemas = mutableMapOf<String, SchemaTypeInfo>()
    private val identifiers = mutableMapOf<String, JsonSchema>()

    init {
        registerWithIdentifier("JsonSchema", JsonSchema.schema, noOutput = true)
    }

    val size: Int get() = schemas.size

    val registeredSchemas: List<SchemaTypeInfo>
        get() = schemas.values.toList()
            .filter { it !is SchemaTypeInfo.SubType }

    fun get(sourceName: String): SchemaTypeInfo? = schemas[sourceName]

    fun findByIdentifier(identifier: String): SchemaEntry {
        val schema = identifiers[identifier]

        if (schema == null) {
            val registeredSchema = registeredSchemas.find {
                when (it) {
                    is SchemaTypeInfo.Obj -> it.schema.sourceName == identifier
                    is SchemaTypeInfo.Enum -> it.schema.enumSourceName == identifier
                    else -> false
                }
            } ?: return SchemaEntry(null, null)
            return SchemaEntry(registeredSchema.schema, registeredSchema)
        }

        val typeInfo =
            if (schema is ObjectJsonSchema) get(schema.sourceName)
            else if (schema is StringJsonSchema && schema.enumSourceName != null) get(schema.enumSourceName!!)
            else null
        return SchemaEntry(schema, typeInfo)
    }

    fun register(schema: JsonSchema?, input: Boolean = false, noOutput: Boolean = false) {
        if (schema == null) return

        when (schema) {
            is ObjectJsonSchema -> registerObject(schema, input, noOutput)
            is StringJsonSchema -> registerEnum(schema, input, noOutput)
            is ArrayJsonSchema -> register(schema.items, input, noOutput)
            else -> {}
        }
    }

    private fun registerObject(schema: ObjectJsonSchema, input: Boolean, noOutput: Boolean) {
        if (seen.contains(schema.sourceName)) {
            // Make sure we update already registered schemas when we find out they're also used for input
            val existing = schemas[schema.sourceName]!!
            if (input && !existing.input) schemas[schema.sourceName] = existing.copyAsInput()

            return
        } else seen.add(schema.sourceName)

        val dependsOn = mutableSetOf<String>()
        for (property in schema.properties) {
            processSubSchema(dependsOn, property.schema, input, noOutput)
        }

        val typeInfo = SchemaTypeInfo.Obj(schema, dependsOn, noOutput, input)

        // Register subtypes with the supertype type info
        if (schema.subTypes != null) {
            for (subType in schema.subTypes!!) {
                if (subType !is ObjectJsonSchema) continue
                else if (seen.contains(subType.sourceName)) continue
                seen.add(subType.sourceName)

                for (property in subType.properties) {
                    processSubSchema(dependsOn, property.schema, input, noOutput)
                }

                // TODO Recursively register subtypes
                schemas[subType.sourceName] = SchemaTypeInfo.SubType(subType, typeInfo, input)
            }
        }

        if (schema.polymorphicValue == null) schemas[schema.sourceName] = typeInfo
    }

    fun registerWithIdentifier(
        identifier: String,
        schema: JsonSchema,
        input: Boolean = false,
        noOutput: Boolean = false
    ) {
        register(schema, input, noOutput)
        identifiers[identifier] = schema
    }

    private fun processSubSchema(dependsOn: MutableSet<String>, schema: JsonSchema, input: Boolean, noOutput: Boolean) {
        if (schema is ObjectJsonSchema) {
            registerObject(schema, input, noOutput)
            dependsOn.add(schema.sourceName)
        } else if (schema is ArrayJsonSchema) {
            processSubSchema(dependsOn, schema.items, input, noOutput)
        } else if (schema is StringJsonSchema) {
            registerEnum(schema, input, noOutput)
            if (schema.enumSourceName != null && schema.enumValues != null) {
                dependsOn.add(schema.enumSourceName!!)
            }
        }
    }

    private fun registerEnum(schema: StringJsonSchema, input: Boolean, noOutput: Boolean) {
        if (schema.enumValues == null) return
        // Those are considered strings, not enums, because in this case, enum values are only used for validation
        // Hence, no registration for those
        val enumSourceName = schema.enumSourceName ?: return

        if (seen.contains(enumSourceName)) return
        else seen.add(enumSourceName)

        schemas[enumSourceName] = SchemaTypeInfo.Enum(schema, emptySet(), noOutput, input)
    }
}
