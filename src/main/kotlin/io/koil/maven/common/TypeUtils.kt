package io.koil.maven.common

import io.koil.schemas.*
import org.apache.maven.plugin.MojoExecutionException

fun jsTypeFromSchema(namePrefix: String, schema: JsonSchema): String {
    return when (schema) {
        is StringJsonSchema -> {
            if (schema.enumSourceName == null) "string"
            else namePrefix + schema.enumSourceName!!
        }

        is IntegerJsonSchema -> "number"
        is FloatJsonSchema -> "number"
        is BooleanJsonSchema -> "boolean"
        is ArrayJsonSchema -> jsTypeFromSchema(namePrefix, schema.items) + "[]"
        is AnyJsonSchema -> "any"

        is ObjectJsonSchema -> {
            if (schema.raw) "object" // TODO Do we have any better ?
            else if (schema.properties.isEmpty() && schema.additionalPropertiesKeys != null && schema.additionalPropertiesValues != null) {
                "Record<" +
                        jsTypeFromSchema(namePrefix, schema.additionalPropertiesKeys!!) +
                        ", " +
                        jsTypeFromSchema(namePrefix, schema.additionalPropertiesValues!!) +
                        ">"
            } else namePrefix + schema.sourceName
        }

        is RefJsonSchema -> throw MojoExecutionException("Refs must be resolved")
    }
}
