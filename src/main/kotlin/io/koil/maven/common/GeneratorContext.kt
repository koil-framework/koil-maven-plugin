package io.koil.maven.common

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.asTypeName
import io.koil.schemas.*
import io.koil.web.webdef.Endpoint
import org.apache.maven.plugin.MojoExecutionException

class GeneratorContext(
    val schemaRegistry: SchemaRegistry,
    private val basePackage: String,
    val namePrefix: String,
) {
    fun modelClassName(sourceName: String): ClassName = ClassName("$basePackage.model", namePrefix + sourceName)

    fun controllerClassName(sourceName: String): ClassName = ClassName("$basePackage.controllers", sourceName)
    fun routerFactoryClassName(apiDefinitionName: String): ClassName =
        ClassName("$basePackage.server", apiDefinitionName + "RouterFactory")

    fun clientClassName(apiDefinitionName: String): ClassName =
        ClassName("$basePackage.client", apiDefinitionName + "Client")

    fun register(endpoint: Endpoint) {
        schemaRegistry.register(endpoint.requestBody.schema, true)
        schemaRegistry.register(endpoint.mainResponse.schema)
        for (response in endpoint.errorResponses) schemaRegistry.register(response.schema)

        // Make sure to also register enums in request parameters
        for (param in endpoint.params) schemaRegistry.register(param.schema, true)
    }

    /** All simple schemas are resolved the same way whether it's a propriety or not */
    private fun resolveCommonSchemas(schema: JsonSchema): TypeName {
        return when (schema) {
            is StringJsonSchema ->
                if (schema.implIdentifier != null) ClassName.bestGuess(schema.implIdentifier!!)
                else if (schema.enumSourceName != null) modelClassName(schema.enumSourceName!!)
                else String::class.asTypeName()

            is IntegerJsonSchema ->
                if (schema.int64) Long::class.asTypeName()
                else Int::class.asTypeName()

            is FloatJsonSchema ->
                if (schema.double) Double::class.asTypeName()
                else Float::class.asTypeName()

            is BooleanJsonSchema -> Boolean::class.asTypeName()

            is ObjectJsonSchema -> {
                if (schema.implIdentifier != null) ClassName.bestGuess(schema.implIdentifier!!)
                else if (schema.raw) ObjectNode::class.asTypeName()
                else if (schema.properties.isEmpty() && schema.additionalPropertiesKeys != null && schema.additionalPropertiesValues != null) {
                    Map::class.asTypeName().parameterizedBy(
                        resolvePropertyType(schema, schema.additionalPropertiesKeys!!),
                        resolvePropertyType(schema, schema.additionalPropertiesValues!!)
                    )
                } else modelClassName(schema.sourceName)
            }

            is AnyJsonSchema -> return JsonNode::class.asTypeName()

            else -> throw MojoExecutionException("Unknown JsonSchema subtype $schema")
        }
    }

    fun resolveType(schema: JsonSchema): TypeName = when (schema) {
        is RefJsonSchema -> {
            val refSchema = schemaRegistry.findByIdentifier(schema.identifier).schema
                ?: throw IllegalStateException("Identifier ${schema.identifier} could not be found")
            resolveType(refSchema)
        }

        is ArrayJsonSchema -> {
            val subType = resolveType(schema.items)
                .copy(nullable = schema.nullableItems)
            if (schema.uniqueItems) Set::class.asTypeName().parameterizedBy(subType)
            else List::class.asTypeName().parameterizedBy(subType)
        }

        else -> resolveCommonSchemas(schema)
    }

    fun resolvePropertyType(parentObject: ObjectJsonSchema, schema: JsonSchema): TypeName = when (schema) {
        is RefJsonSchema -> {
            val ref = resolvePropertyRef(schema.identifier, parentObject)
                ?: throw IllegalStateException("Identifier ${schema.identifier} could not be found")
            resolvePropertyType(parentObject, ref)
        }

        is ArrayJsonSchema -> {
            val subType = resolvePropertyType(parentObject, schema.items)
                .copy(nullable = schema.nullableItems)
            if (schema.uniqueItems) Set::class.asTypeName().parameterizedBy(subType)
            else List::class.asTypeName().parameterizedBy(subType)
        }

        else -> resolveCommonSchemas(schema)
    }

    fun resolvePropertyRef(identifier: String, parentObject: ObjectJsonSchema): JsonSchema? {
        if (identifier == "this") return parentObject
        else if (identifier == "super") {
            val typeInfo = schemaRegistry.get(parentObject.sourceName)
                ?: return null
            if (typeInfo !is SchemaTypeInfo.SubType) return parentObject
            val supertypeInfo = typeInfo.supertypeTypeInfo
            return if (supertypeInfo is SchemaTypeInfo.Obj) supertypeInfo.schema
            else null // TODO Make sure recursion is right for multi-level polymorphic objects
        }

        return schemaRegistry.findByIdentifier(identifier).schema
    }

    private fun resolveCommonJsTypes(schema: JsonSchema): String {
        return when (schema) {
            is StringJsonSchema -> {
                if (schema.enumSourceName == null) "string"
                else namePrefix + schema.enumSourceName!!
            }

            is IntegerJsonSchema -> "number"
            is FloatJsonSchema -> "number"
            is BooleanJsonSchema -> "boolean"
            is AnyJsonSchema -> "any"

            is ObjectJsonSchema -> {
                if (schema.raw) "Record<string, any>"
                else if (schema.properties.isEmpty() && schema.additionalPropertiesKeys != null && schema.additionalPropertiesValues != null) {
                    "Record<" +
                            jsTypeFromSchema(namePrefix, schema.additionalPropertiesKeys!!) +
                            ", " +
                            jsTypeFromSchema(namePrefix, schema.additionalPropertiesValues!!) +
                            ">"
                } else namePrefix + schema.sourceName
            }

            else -> throw MojoExecutionException("Unknown JsonSchema subtype $schema")
        }
    }

    fun resolveJsType(schema: JsonSchema): String {
        return when (schema) {
            is RefJsonSchema -> {
                val refSchema = schemaRegistry.findByIdentifier(schema.identifier).schema
                    ?: throw IllegalStateException("Identifier ${schema.identifier} could not be found")
                resolveJsType(refSchema)
            }

            is ArrayJsonSchema -> {
                val resolved = resolveJsType(schema.items)
                if (!schema.nullableItems) "$resolved[]"
                else "($resolved | null)[]"
            }

            else -> resolveCommonJsTypes(schema)
        }
    }

    fun resolvePropertyJsType(parentObject: ObjectJsonSchema, schema: JsonSchema): String = when (schema) {
        is RefJsonSchema -> {
            val ref = resolvePropertyRef(schema.identifier, parentObject)
                ?: throw IllegalStateException("Identifier ${schema.identifier} could not be found")
            resolvePropertyJsType(parentObject, ref)
        }

        is ArrayJsonSchema -> {
            val resolved = resolvePropertyJsType(parentObject, schema.items)
            if (!schema.nullableItems) "$resolved[]"
            else "($resolved | null)[]"
        }

        else -> resolveCommonJsTypes(schema)
    }

    fun resolveSubType(parent: ObjectJsonSchema, subType: JsonSchema): ObjectJsonSchema =
        if (subType is RefJsonSchema) {
            val ref = resolvePropertyRef(subType.identifier, parent)
                ?: throw IllegalStateException("Identifier ${subType.identifier} could not be found")
            if (ref !is ObjectJsonSchema) throw IllegalStateException("Subtype referenced through identifier ${subType.identifier} is not an object")
            else if (ref.polymorphicValue == null) throw IllegalStateException("Subtype referenced through identifier ${subType.identifier} does not have a polymorphic value")
            ref
        } else {
            subType as ObjectJsonSchema
        }
}
