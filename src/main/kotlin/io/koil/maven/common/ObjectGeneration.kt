package io.koil.maven.common

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.squareup.kotlinpoet.*
import io.koil.schemas.JsonSchemaObjectProperty
import io.koil.schemas.ObjectJsonSchema
import io.koil.schemas.StringJsonSchema

fun generateEnum(ctx: GeneratorContext, enum: StringJsonSchema): FileSpec? {
    // Don't write enums with an impl identifier
    if (enum.implIdentifier != null) return null

    // Enums will have a source name here because they wouldn't be registered if they didn't
    // No null check here is fine

    val className = ctx.modelClassName(enum.enumSourceName!!)

    val type = TypeSpec.enumBuilder(className)
    for (value in enum.enumValues!!) {
        // TODO  Make sure there are no illegal chars in the enum values
        type.addEnumConstant(value.uppercase())
    }

    return FileSpec.builder(className)
        .addType(type.build())
        .build()
}

private fun createParam(prop: JsonSchemaObjectProperty, type: TypeName): ParameterSpec {
    val param = ParameterSpec.builder(prop.sourceName, type)
    if (prop.sourceName != prop.name) param.addAnnotation(
        AnnotationSpec.builder(JsonProperty::class)
            .addMember("%S", prop.name)
            .build()
    )
    if (prop.aliases.isNotEmpty()) param.addAnnotation(
        AnnotationSpec.builder(JsonAlias::class)
            .addMember("[]", prop.aliases)
            .build()
    )
    if (prop.schema.description != null) param.addKdoc(prop.schema.description!!)
    return param.build()
}

private fun createProp(prop: JsonSchemaObjectProperty, type: TypeName, override: Boolean): PropertySpec {
    val p = PropertySpec.builder(prop.sourceName, type)
        .initializer(prop.sourceName)
    if (override) p.addModifiers(KModifier.OVERRIDE)
    return p.build()
}

fun generateObject(ctx: GeneratorContext, obj: ObjectJsonSchema): FileSpec? {
    // If this is a raw object
    if (obj.implIdentifier != null || obj.raw) return null
    // If this is a map
    if (obj.properties.isEmpty() && obj.additionalPropertiesKeys != null && obj.additionalPropertiesValues != null) return null

    val className = ctx.modelClassName(obj.sourceName)

    if (obj.subTypes != null) {
        val subTypes = mutableListOf<TypeSpec>()
        for (subType in obj.subTypes!!) {
            subTypes.add(generateSubType(ctx, obj, ctx.resolveSubType(obj, subType)))
        }

        return FileSpec.builder(className)
            .addType(generateSupertype(ctx, obj))
            .addTypes(subTypes)
            .build()
    }

    return FileSpec.builder(className)
        .addType(generatePojo(ctx, obj))
        .build()
}

private fun generateSupertype(ctx: GeneratorContext, obj: ObjectJsonSchema): TypeSpec {
    val polymorphicKey = obj.properties.firstOrNull { it.polymorphicKey }
        ?: throw IllegalStateException("Type ${obj.sourceName} is declared as a supertype but does not have a polymorphic key declared")

    val type = TypeSpec.interfaceBuilder(ctx.modelClassName(obj.sourceName))
        .addKdoc(obj.description ?: "")
        .addAnnotation(
            AnnotationSpec.builder(JsonTypeInfo::class)
                .addMember("use = %T.Id.NAME", JsonTypeInfo::class)
                .addMember("include = %T.As.EXISTING_PROPERTY", JsonTypeInfo::class)
                .addMember("property = %S", polymorphicKey.sourceName)
                .addMember("visible = false")
                .build()
        )

    val subTypeAnnotation = AnnotationSpec.builder(JsonSubTypes::class)
    for (subType in obj.subTypes!!) {
        subType as ObjectJsonSchema
        subTypeAnnotation.addMember(
            "%1T.Type(%2L::class, name = %2L.%3L)",
            JsonSubTypes::class,
            ctx.modelClassName(subType.sourceName),
            polymorphicKey.sourceName.camelToSnakeCase().uppercase(),
        )
    }
    type.addAnnotation(subTypeAnnotation.build())

    for (property in obj.properties) {
        type.addProperty(
            PropertySpec.builder(
                property.sourceName,
                ctx.resolvePropertyType(obj, property.schema).copy(nullable = property.nullable)
            ).addKdoc(property.schema.description ?: "").build()
        )
    }

    // TODO Add support for additional properties

    return type.build()
}

private fun generateSubType(ctx: GeneratorContext, supertype: ObjectJsonSchema, obj: ObjectJsonSchema): TypeSpec {
    val typeSpec = TypeSpec.classBuilder(ctx.modelClassName(obj.sourceName))
        .addModifiers(KModifier.DATA)
        .addSuperinterface(ctx.modelClassName(supertype.sourceName))

    val companion = TypeSpec.companionObjectBuilder()
    val ctorSpec = FunSpec.constructorBuilder()

    for (prop in supertype.properties) {
        val type = ctx.resolvePropertyType(supertype, prop.schema).copy(nullable = prop.nullable)

        if (!prop.polymorphicKey) {
            ctorSpec.addParameter(createParam(prop, type))
            typeSpec.addProperty(createProp(prop, type, true))
            continue
        }

        val typeVarName = prop.sourceName.camelToSnakeCase().uppercase()
        val typeVar = PropertySpec.builder(typeVarName, type)
        val schema = prop.schema
        if (schema is StringJsonSchema && schema.enumSourceName != null) {
            typeVar.initializer("%T.%L", ctx.modelClassName(schema.enumSourceName!!), obj.polymorphicValue)
        } else {
            typeVar.addModifiers(KModifier.CONST)
            typeVar.initializer("%S", obj.polymorphicValue)
        }

        companion.addProperty(typeVar.build())
        typeSpec.addProperty(
            PropertySpec.builder(prop.sourceName, type, KModifier.OVERRIDE)
                .initializer(typeVarName)
                .build()
        )
    }

    for (prop in obj.properties) {
        val type = ctx.resolvePropertyType(obj, prop.schema).copy(nullable = prop.nullable)
        ctorSpec.addParameter(createParam(prop, type))
        typeSpec.addProperty(createProp(prop, type, false))
    }

    // TODO Add support for additional properties

    return typeSpec.primaryConstructor(ctorSpec.build())
        .addType(companion.build())
        .build()
}

private fun generatePojo(ctx: GeneratorContext, obj: ObjectJsonSchema): TypeSpec {
    val typeSpec = TypeSpec.classBuilder(ctx.modelClassName(obj.sourceName))
        .addModifiers(KModifier.DATA)
        .addKdoc(obj.description ?: "")

    val ctorSpec = FunSpec.constructorBuilder()

    for (prop in obj.properties) {
        val type = ctx.resolvePropertyType(obj, prop.schema).copy(nullable = prop.nullable)
        ctorSpec.addParameter(createParam(prop, type))
        typeSpec.addProperty(createProp(prop, type, false))
    }

    // TODO Add support for additional properties

    return typeSpec.primaryConstructor(ctorSpec.build())
        .build()
}
